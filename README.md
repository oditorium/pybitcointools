# PyBitcoinTools Revisited

## About

This is an updated version of Vitalik Buterin's `pybitcointools` library, which serves the two following purposes

- it should work under Python 3 (Vitalik's code falls over a lot when
used with Python 3)

- it should be easy to use for a beginner (Vitalik's code is a
toolkit of separate functions, but if you don't know how to chain
them together you are on your own)

Note that I only ported a portion of the code--Vitalik's code does
much more than mine. At one point I might port some things if and
when I need them, and of course pull requests are always welcome.

This repo contains two distinct Python packages

- `bitcoin` which is Vitalik's original code, with some minor changes
that deal with making it work under Python 3 (they might however
break under Python 2)

- `btc` which is my refactored and annotated version of Vitalik's
code (which now certainly no longer works under Python 2) plus some
objects that wrap it for easier use

Vitalik's README and LICENSE files are still available as
`READMEVB.md` and `LICENSEMD` respectively, and they are relevant for
everything in that part of the repo.

## Structure

The main package is called `btc`. It has the following sub packages

- `keys` for key management
- `msgs` for message and signature management
- `txns` for transaction management
- `nodes` for connecting to nodes, including web APIs

It has also two top-level modules

- `technical` for all kind of technical functions
- `const` for constants and exceptions

Normally it is enough to import the `btc` package and use the other
packages as follows

    import btc

    privkey = "..."
    mykey = btc.keys.Key(privkey, testnet=True)
    mymessage = btc.msgs.Message("Hello World")
    mytxn = btx.txns.Transaction()
    ...
    btc.nodes.apit(btc.BLOCKCYPHER).send_tx(mytxn.txn)

There is not usually a need to import the `technical` and `constants`
modules directly (note that specifically constants are available
through every major module in the package).

## Examples

There are a number of examples in the Jupyter notebooks at the root
level of the project


## Copyright, License & Warranty

Vitalik's code--ie everything in the `bitcoin` package--is under an
MIT license. My code--ie everything under the `btc` package--is under
a Mozilla Public License v2.0, meaning essentially code can only be
used if changes are republished on a per-file basis (for details
please refer to the license text in the LICENSE file or
[here](https://www.mozilla.org/en-US/MPL/2.0/)).

It goes without saying that **THIS CODE COMES WITH NO WARRANTY AT ALL,
YOU USED IT AT YOUR OWN RISK**.

Copyright (c) Stefan LOESCH 2017. All rights reserved.
