"""
Technical functions for Bitcoin tools

You probably do not need those, but here they are anyway; they are
based on (and in many instances directly copied from) Vitalik Buterin's
`pybitcointools` toolkit.

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""
import hashlib
import re
import binascii
import os

from .const import WrongKeyFormat

import logging
logger = logging.getLogger(__name__)


####################################################################
## ELLIPTIC CURVE PARAMETERS (secp256k1)

# TODO: move them into one of the files

kP = 2**256 - 2**32 - 977
kN = 115792089237316195423570985008687907852837564279074904382605163141518161494337
kA = 0
kB = 7
kGx = 55066263022277343669578718895168534326250603453777594175500187360389116729240
kGy = 32670510020758816978083085130507043184471273380659243275938904335757337482424
kG = (kGx, kGy)

# called P, N, A, B, Gx, Gy, G respectively in Vitalik's code



########################################################################
## JACOBIAN FUNCTIONS AND OPERATIONS

####################################
## INV
def inv(a, n):
    """
    inverse (using extended Euclidean algorithm)
    """
    # called inv in Vitalik's code

    if a == 0:
        return 0
    lm, hm = 1, 0
    low, high = a % n, n
    while low > 1:
        r = high//low
        nm, new = hm-lm*r, high-low*r
        lm, low, hm, high = nm, new, lm, low
    return lm % n

####################################
## TO JACOBIAN
def to_jacobian(p):
    # called to_jacobian in Vitalik's code

    o = (p[0], p[1], 1)
    return o


####################################
## JACOBIAN DOUBLE
def jacobian_double(p):
    # called jacobian_double in Vitalik's code

    if not p[1]:
        return (0, 0, 0)
    ysq = (p[1] ** 2) % kP
    S = (4 * p[0] * ysq) % kP
    M = (3 * p[0] ** 2 + kA * p[2] ** 4) % kP
    nx = (M**2 - 2 * S) % kP
    ny = (M * (S - nx) - 8 * ysq ** 2) % kP
    nz = (2 * p[1] * p[2]) % kP
    return (nx, ny, nz)


####################################
## JACOBIAN ADD
def jacobian_add(p, q):
    # called jacobian_add in Vitalik's code

    if not p[1]: return q
    if not q[1]: return p
    U1 = (p[0] * q[2] ** 2) % kP
    U2 = (q[0] * p[2] ** 2) % kP
    S1 = (p[1] * q[2] ** 3) % kP
    S2 = (q[1] * p[2] ** 3) % kP
    if U1 == U2:
        if S1 != S2: return (0, 0, 1)
        return jacobian_double(p)
    H = U2 - U1
    R = S2 - S1
    H2 = (H * H) % kP
    H3 = (H * H2) % kP
    U1H2 = (U1 * H2) % kP
    nx = (R ** 2 - H3 - 2 * U1H2) % kP
    ny = (R * (U1H2 - nx) - S1 * H3) % kP
    nz = (H * p[2] * q[2]) % kP
    return (nx, ny, nz)


####################################
## FROM JACOBIAN
def from_jacobian(p):
    # called from_jacobian in Vitalik's code

    z = inv(p[2], kP)
    return ((p[0] * z**2) % kP, (p[1] * z**3) % kP)


####################################
## JACOBIAN MULTIPLY
def jacobian_multiply(a, n):
    # called jacobian_multiply in Vitalik's code

    if a[1] == 0 or n == 0: return (0, 0, 1)
    if n == 1: return a
    if n < 0 or n >= kN: return jacobian_multiply(a, n % kN)
    if (n % 2) == 0: return jacobian_double(jacobian_multiply(a, n//2))
    if (n % 2) == 1: return jacobian_add(jacobian_double(jacobian_multiply(a, n//2)), a)


####################################
## FAST MULTIPLY
def fast_multiply(a, n):
    # called fast_multiply in Vitalik's code

    return from_jacobian(jacobian_multiply(to_jacobian(a), n))


####################################
## FAST ADD
def fast_add(a, b):
    # called fast_multiply in Vitalik's code

    return from_jacobian(jacobian_add(to_jacobian(a), to_jacobian(b)))




########################################################################
## CONVERSIONS

####################################
## SAFE HEXLIFY
def safe_hexlify(a):
    """
    returns hex representation of bytes string (as string)
    """
    # called safe_hexlify in Vitalik's code

    return str(binascii.hexlify(a), 'utf-8')

####################################
## BYTES TO HEX STRING
def bytes_to_hex_string(b):
    """
    converts bytes to a hex string
    """
    # called bytes_to_hex_string in Vitalik's code

    if isinstance(b, str): return b
    return ''.join('{:02x}'.format(y) for y in b)


####################################
## SAFE FROM HEX (private)
def _safe_from_hex(s):
    # called safe_from_hex in Vitalik's code
    return bytes.fromhex(s)


####################################
## FROM INT REPRESENTATION TO BYTES
def from_int_representation_to_bytes(a):
    # called from_int_representation_to_bytes in Vitalik's code
    return bytes(str(a), 'utf-8')


####################################
## FROM INT TO BYTE
def from_int_to_byte(a):
    return bytes([a])


####################################
## FROM BYTE TO INT
def from_byte_to_int(a):
    # called from_byte_to_int in Vitalik's code
    return a


####################################
## FROM STRING TO BYTES
def from_string_to_bytes(a):
    """
    converts string to bytes
    """
    # called from_string_to_bytes in Vitalik's code

    return a if isinstance(a, bytes) else bytes(a, 'utf-8')


####################################
## HASH TO INT
def hash_to_int(x):
    """
    converts hash to int
    """
    # called hash_to_int in Vitalik's code

    if len(x) in [40, 64]: return decode(x, 16)
    return decode(x, 256)


####################################
## NUM TO VAR INT
def num_to_var_int(x):
    """
    convert a number to a variable length int (returns bytes)

    NOTE: length of int is encoded in first byte
    - <253 -> 1 byte
    - =253 -> 1+2byte (65k)
    - =254 -> 1+4byte (4b)
    - =255 -> 1+8byte (16bb)
    """
    # called num_to_var_int in Vitalik's code

    x = int(x)
    if x < 253: return from_int_to_byte(x)
    elif x < 65536: return from_int_to_byte(253)+encode(x, 256, 2)[::-1]
    elif x < 4294967296: return from_int_to_byte(254) + encode(x, 256, 4)[::-1]
    else: return from_int_to_byte(255) + encode(x, 256, 8)[::-1]




########################################################################
## HASHES

####################################
## HASH160 BIN
def hash160_bin(bstring):
    """
    computes ripemd160 of sha256 (takes and returns bytes string)
    """
    # called bin_hash160 in Vitalik's code

    intermed = hashlib.sha256(bstring).digest()
    #digest = ''
    #try:
    digest = hashlib.new('ripemd160', intermed).digest()
    #except:
    #    digest = RIPEMD160(intermed).digest()
    return digest


####################################
## HASH160 HEX
def hash160_hex(bstring):
    """
    computes RIPEMD160 of SHA256 (takes bytes string, returns hex)
    """
    # called hash160 in Vitalik's code

    return safe_hexlify(hash160_bin(bstring))


####################################
## BIN SHA256
def sha256_bin(bstring):
    """
    computes SHA256 (takes bytes string, returns bytes string)
    """
    # called binsha256 in Vitalik's code

    binary_data = string if isinstance(bstring, bytes) else bytes(bstring, 'utf-8')
    return hashlib.sha256(binary_data).digest()


####################################
## SHA256
def sha256_hex(bstring):
    """
    computes SHA256 (takes bytes string, returns hex)
    """
    # called sha256 in Vitalik's code

    return bytes_to_hex_string(sha256_bin(bstring))


####################################
## RIPEMID160 BIN
def ripemd160_bin (bstring):
    """
    computes RIPEMD160 (takes and returns bytes string)
    """
    # called bin_ripemd160 in Vitalik's code

    #try:
    digest = hashlib.new('ripemd160', bstring).digest()
    #except:
    #    digest = RIPEMD160(string).digest()
    return digest


####################################
## RIPEMD160 HEX
def ripemd160_hex(string):
    """
    computes RIPEMD160 (takes bytes string, returns hex)
    """
    # called ripemd160 in Vitalik's code

    return safe_hexlify(_bin_ripemd160(string))


####################################
## BIN DBL SHA256 (private)
def bin_dbl_sha256_hex(s):
    """
    calculates SHA256 of SHA256 ("double SHA"; bytes output)
    """
    # called bin_dbl_sha256 in Vitalik's code
    bytes_to_hash = from_string_to_bytes(s)
    return hashlib.sha256(hashlib.sha256(bytes_to_hash).digest()).digest()


####################################
## DBL SHA 256
def dbl_sha256_hex(string):
    """
    calculates SHA256 of SHA256 ("double SHA"; hex output)
    """
    # called dbl_sha256 in Vitalik's code
    return safe_hexlify(bin_dbl_sha256_hex(string))


####################################
## BIN SLOWSHA
def slowsha_bin(string):
    """
    calculate SHA256 slowly (bytes output)
    """
    # called bin_slowsha in Vitalik's code
    string = from_string_to_bytes(string)
    orig_input = string
    for i in range(100000):
        string = hashlib.sha256(string + orig_input).digest()
    return string


####################################
## SLOWSHA (private)
def slowsha_hex(string):
    """
    calculate SHA256 slowly (hex output)
    """
    # called slowsha in Vitalik's code

    return safe_hexlify(slowsha_bin(string))




########################################################################
## TESTS

####################################
## BIN DBL SHA256 HEX
def bin_dbl_sha256_hex(s):
    # called bin_dbl_sha256 in Vitalik's code

    bytes_to_hash = from_string_to_bytes(s)
    return hashlib.sha256(hashlib.sha256(bytes_to_hash).digest()).digest()




########################################################################
## ENCODING / DECODING

####################################
## CODE STRINGS (private)
_CODE_STRINGS = {
    2: '01',
    10: '0123456789',
    16: '0123456789abcdef',
    32: 'abcdefghijklmnopqrstuvwxyz234567',
    58: '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz',
    256: ''.join([chr(x) for x in range(256)])
}


####################################
## GET CODE STRING
def get_code_string(base):
    """
    gets the code string for a given numeric base

    base 10: 0..9
    base 16: 0..9a..f
    base 58: 1..9A..Za..z
    ...
    """
    # called get_code_string in Vitalik's code

    if base in _CODE_STRINGS: return _CODE_STRINGS[base]
    else: raise ValueError("Invalid base!")

####################################
## LPAD
def lpad(msg, symbol, length):
    """
    left pad
    """
    # called lpad in Vitalik's code

    if len(msg) >= length: return msg
    return symbol * (length - len(msg)) + msg


####################################
## DECODE
def decode(string, base):
    """
    decodes a number string which is given in a specific base

    base 10: decimal
    base 16: hex
    base 58: 1..9A..Za..z
    """
    # called decode in Vitalik's code

    if base == 256 and isinstance(string, str):
        string = bytes(bytearray.fromhex(string))
    base = int(base)
    code_string = get_code_string(base)
    result = 0
    if base == 256:
        def extract(d, cs):
            return d
    else:
        def extract(d, cs):
            return cs.find(d if isinstance(d, str) else chr(d))

    if base == 16:
        string = string.lower()
    while len(string) > 0:
        result *= base
        result += extract(string[0], code_string)
        string = string[1:]
    return result


####################################
## ENCODE
def encode(val, base, minlen=0):
    """
    encodes a number to a certain base

    base 10: decimal
    base 16: hex
    base 58: 1..9A..Za..z
    ...
    """
    # called encode in Vitalik's code

    base, minlen = int(base), int(minlen)
    code_string = get_code_string(base)
    result_bytes = bytes()
    while val > 0:
        curcode = code_string[val % base]
        result_bytes = bytes([ord(curcode)]) + result_bytes
        val //= base

    pad_size = minlen - len(result_bytes)

    padding_element = b'\x00' if base == 256 else b'1' \
        if base == 58 else b'0'
    if (pad_size > 0):
        result_bytes = padding_element*pad_size + result_bytes

    result_string = ''.join([chr(y) for y in result_bytes])
    result = result_bytes if base == 256 else result_string

    return result


####################################
## CHANGE BASE
def change_base(string, frm_base, to_base, minlen=0):
    """
    reencode string to a new base

    base 10: decimal
    base 16: hex
    base 58: 1..9A..Za..z
    ...
    """
    # called changebase in Vitalik's code

    if frm_base == to_base:
        return lpad(string, get_code_string(frm_base)[0], minlen)
    return encode(decode(string, frm_base), to_base, minlen)


####################################
## B58CHECK TO BIN
def b58check_to_bin(inp):
    """
    convert B58-checked format to binary
    """
    # called b58check_to_bin in Vitalik's code

    leadingzbytes = len(re.match('^1*', inp).group(0))
    data = b'\x00' * leadingzbytes + change_base(inp, 58, 256)
    if not bin_dbl_sha256_hex(data[:-4])[:4] == data[-4:]:
        raise WrongKeyFormat("Invalid B58 Format")
    return data[1:-4]


####################################
## B58CHECK TO HEX
def b58check_to_hex(inp):
    """
    convert B58-checked format to hex
    """
    # called b58check_to_hex in Vitalik's code

    return safe_hexlify(b58check_to_bin(inp))


####################################
## BIN TO B58CHECK
def bin_to_b58check(inp, magicbyte=0):
    """
    convert binary to B58-checked

    magicbyte indicates testnet (=111)
    """
    # called bin_to_b58check in Vitalik's code

    if magicbyte == 0:
        inp = from_int_to_byte(0) + inp
    while magicbyte > 0:
        inp = from_int_to_byte(magicbyte % 256) + inp
        magicbyte //= 256

    leadingzbytes = 0
    for x in inp:
        if x != 0: break
        leadingzbytes += 1

    checksum = bin_dbl_sha256_hex(inp)[:4]
    return '1' * leadingzbytes + change_base(inp+checksum, 256, 58)




########################################################################
## OTHER

####################################
## RANDOM STRING
def random_string(x):
    """
    creates a random string of lenght `x`
    """
    # called random_string in Vitalik's code

    return str(os.urandom(x))
