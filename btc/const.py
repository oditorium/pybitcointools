"""
Constants for Bitcoin tools

you would not usually import those directly, they are imported into
the module where needed

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""


##########################################################
## EXCEPTIONS

# general
class WrongParameters(ValueError):pass          # some of the parameters supplied are wrong
class InternalError(ValueError):pass            # something is wrong, but related to the code
class ObjectLocked(RuntimeError): pass          # there is an attempt to change a locked object
class NoIterables(NotImplementedError): pass    # an iterable has been given, but function only accepts single items

# keys
class WrongKeyType(WrongParameters): pass
class WrongKeyFormat(WrongParameters): pass
class WrongPrivateKeyFormat(WrongKeyFormat): pass
class WrongPublicKeyFormat(WrongKeyFormat): pass
class WrongAddressFormat(WrongKeyFormat): pass

class KeyNotInChain(KeyError): pass

# msgs
class InsufficientDataForVerification(ValueError): pass
class MissingVerificationSignature(InsufficientDataForVerification): pass
class MissingVerificationKey(InsufficientDataForVerification): pass
class WrongParameters(ValueError): pass
class SignatureFailedVerification(ValueError): pass

# txns
class TransactionObjectLocked(ObjectLocked): pass

# txns.impl
class WrongOutputsFormat(WrongParameters): pass
class InsufficientFunds(ValueError): pass
class MissingChangeAddress(ValueError): pass

# node
class WrongNetwork(ValueError): pass
class NotOnTestnet(WrongNetwork): pass
class NotOnMainnet(WrongNetwork): pass

class APIRequestError(RuntimeError): pass
class ErrorResponse(APIRequestError): pass



##########################################################
# GENERAL
SATOSHIS_PER_BITCOIN = 100000000

# btcs (method)
def btcs(btc):
    """
    convert number of bitcoins into satoshis
    """
    return int(btc * SATOSHIS_PER_BITCOIN)

# btcnts (method)
def btcnts(btcnt):
    """
    convert number of bitcentimes (=0.01btc) into satoshis
    """
    return int(0.01 * btcnt * SATOSHIS_PER_BITCOIN)

# to_btc (method)
def to_btcs(btc):
    """
    convert number of satoshis into bitcoin
    """
    return float(btc)/SATOSHIS_PER_BITCOIN

# to_btcnts (method)
def to_btcnts(btc):
    """
    convert number of satoshis into bitcentimes (=0.01btc)
    """
    return float(btc)/SATOSHIS_PER_BITCOIN*100



##########################################################
# KEY FORMAT SPECIFICATIONS
ADDRESSF=1                          # standard address format
ADDRF=ADDRESSF
DECIMAL = 2                         # integer
DEC = DECIMAL
BIN = 4                             # byte string
BIN_COMPRESSED = 5
BINC = BIN_COMPRESSED
HEX = 6                             # hex string
HEX_COMPRESSED = 7
HEXC = HEX_COMPRESSED
WIF = 8                             # wallet interchange
B58 = WIF                           # = base 58 with check sum
WIF_COMPRESSED = 9
WIFC = WIF_COMPRESSED
B58_COMPRESSED = WIF_COMPRESSED
B58C = WIF_COMPRESSED
ELECTRUM_BIN = 20                   # electrum byte string
EBIN = ELECTRUM_BIN
ELECTRUM_HEX = 18                   # electrum  hex string
EHEX = ELECTRUM_HEX

KEY_FORMAT_NAMES = {
    ADDRF:          'ADDR',
    DEC:            'DEC',
    BIN:            'BIN',
    BINC:           'BINC',
    HEX:            'HEX',
    HEXC:           'HEXC',
    WIF:            'WIF',      # b58c
    WIFC:           'WIFC',
    EBIN:           'EBIN',     # electrum bin
    EHEX:           'EHEX',     # electrum hex
}


##########################################################
# KEY TYPES
ADDRESS = 1                     # address only
ADDR = ADDRESS
PUBLIC = 3                      # public key and address only
PUB = PUBLIC
PRIVATE = 5                     # private, public key and address
PRIV = PRIVATE

KEY_TYPE_NAMES = {
    PRIVATE:    'PRIV',
    PUBLIC:     'PUBL',
    ADDR:       'ADDR',
}


##########################################################
# Network
MAINNET = 1                     # the main bitcoin network
BITCOIN = MAINNET
TESTNET = 2                     # the bitcoin test net

NETWORK_NAMES = {
    MAINNET: 'MAINNET',
    TESTNET: 'TESTNET',
}

##########################################################
# TRANSACTIONS
SIGHASH_ALL = 1
SIGHASH_NONE = 2
SIGHASH_SINGLE = 3
# this works like SIGHASH_ANYONECANPAY | SIGHASH_ALL, might as
# well make it explicit while we fix the constant
SIGHASH_ANYONECANPAY = 0x81


# json_change_base
CHANGE_BASE_TO_HEX    = 0
CHANGE_BASE_TO_HEXB   = 1
CHANGE_BASE_FROM_HEX  = 2


##########################################################
# NODE

# providers
BLOCKEXPLORER = 1
BLOCKCYPHER   = 2
