"""
Bitcoin transaction management module, class AnalyzeTransaction

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""
from ..const import *
from .impl import *
#from ..keys import Key

from ..nodes import api_get, BLOCKEXPLORER

from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)

####################################################################
## CLASS ANALYZE TRANSACTION
class AnalyzeTransaction():
    """
    analyzes an existing transaction, either from raw data, or by
    querying the respective APIs on BlockExplorer and BlockCypher
    """


    ####################################
    ## CONSTRUCTOR
    def __init__(s, txnhash=None, network=MAINNET, outix=None, rawtxn=None):
        """
        constructor

        NOTE
        - if `rawtxn` is given, this initialises the transaction object

        - alternatively, `txhash` can be given, in which case the
          transaction is taken from the chain (using `retrieve_tx` which
          can be overwritten by a derived class if desired)

        - `network` is either MAINNET or TESTNET, depending on the chain

        - `outix` is the output index for which this transaction has been
          retrieved by the `retrieve`
        """

        if not rawtxn is None and not txnhash is None:
            raise WrongParameters("cant provide both `rawtxn` and `txnhash`")

        if not rawtxn is None:
            s._txnhash = None
            s._network = None
            s._rawtxn = rawtxn

        if not txnhash is None:
            s._txnhash = txnhash
            s._network = network
            s._rawtxn   = s.retrieve_rawtxn(txnhash, network)

        s._txno_bex = None
        s._txno_bcy = None
        s._outix = outix

        s._txno = deserialize(s.rawtxn)


    ####################################
    ## FROM TXNHASH (factory)
    @classmethod
    def from_txnhash(cls, txnhash, network=None):
        """
        create object from transaction id

        NOTES
        - if the network is not given then it is implied from the
          (double-colon-separated prefix of) the transaction hash

            - "e06a6950...", "M/e06a6950...", "MAIN/e06a6950...", and
              "MAINNET/e06a6950..." is main net

            - "e06a6950...", "T/e06a6950...", "TEST/e06a6950...", and
              "TESTNET/e06a6950..." is test net

        - if the transaction has contains a single colon then the
          transaction is considered an outpoint, and the corresponding
          transaction hash is passed as `outix` to the constructor

             - "e06a6950...aa:1" indicates output #1 if this transaction
        """

        # find a prefix for the network if there is none
        # (separated by '::')
        if network is None:
            txnhash_split = txnhash.split("/")
            if len(txnhash_split) == 1:
                # no '/' in txnhash -> main net
                network = MAINNET
            else:
                nw = txnhash_split[0].upper()
                txnhash = txnhash_split[1]
                if nw == "MAIN" or nw == "M" or nw == "MAINNET":
                    network = MAINNET
                elif nw == "TEST" or nw == "T" or nw == "TESTNET":
                    network = TESTNET
                else:
                    raise WrongParameters ("prefix must be MAINNET or TESTNET (prefix='{}')".format(nw))

        # now look for an output index
        txnhash_split = txnhash.split(":")
        if len(txnhash_split) > 1:
            txnhash = txnhash_split[0]
            index = int(txnhash_split[1])
        else:
            index = None

        return cls(txnhash=txnhash, network=network, outix=index)

    ####################################
    ## __REPR__
    def __repr__(s):
        ""
        parts = OrderedDict()
        if s._txnhash is None:
            parts['rawtxn'] = '"{}"'.format(s._rawtxn)
        else:
            parts['txnhash'] = '"{}"'.format(s._txnhash)
            parts['network'] = s._network
            if not s._outix is None:
                parts['outix'] = s._outix
        result = ", ".join(["{}={}".format(k,v) for k,v in parts.items()])
        result = "{}({})".format(s.__class__.__name__, result)
        return result


    ####################################
    ## __STR__
    def __str__(s):
        ""
        if s._txnhash is None:
            result = 'rawtxn = "{}"...'.format(s._rawtxn[:6])
        else:
            result = s._txnhash[:6]+"..."
            if s._network == TESTNET:result = "TEST/"+result
            if not s._outix is None: result = result + ":" + str(s._outix)
        result = '{}("{}")'.format(s.__class__.__name__, result)
        return result


    ####################################
    ## TXNHASH (property)
    @property
    def txnhash(s):
        """
        getter for the transaction hash (None if raw transaction only)
        """
        return s._txnhash

    ####################################
    ## OUTIX (property)
    @property
    def outix(s):
        """
        returns the output index if given (raises if None)
        """
        if s._outix is None:
            raise WrongParameters("There is not output index associated with this transaction!")
        return s._outix

    ####################################
    ## BLKHASH (property)
    @property
    def blkhash(s):
        """
        associated block hash (from BlockCypher)
        """
        s._assert_bcy()
        return s._txno_bcy['block_hash']

    ####################################
    ## BLKHEIGHT (property)
    @property
    def blkheight(s):
        """
        associated block height (from BlockCypher)
        """
        s._assert_bcy()
        return s._txno_bcy['block_height']

    ####################################
    ## CONFIRMATION TIME (property)
    @property
    def confirmation_time(s):
        """
        associated confirmation time as string (from BlockCypher)
        """
        s._assert_bcy()
        return s._txno_bcy['confirmed']


    ####################################
    ## AGGR VALUE (property)
    @property
    def aggr_values (s):
        """
        aggregate value inputs, outputs, fees (from BlockExplorer)
        """
        s._assert_bex()
        v = {}
        v['ins'] = (s._txno_bex['valueIn'], int(s._txno_bex['valueIn']*SATOSHIS_PER_BITCOIN))
        v['outs'] = (s._txno_bex['valueOut'], int(s._txno_bex['valueOut']*SATOSHIS_PER_BITCOIN))
        v['fees'] = (s._txno_bex['fees'], int(s._txno_bex['fees']*SATOSHIS_PER_BITCOIN))
        return v

    ####################################
    ## AGGR VALUE (property)
    @property
    def values(s):
        """
        detailed value inputs, outputs, fees (from BlockCypher)
        """
        s._assert_bcy()
        v = {}
        v['ins'] = tuple( (i['output_value']/SATOSHIS_PER_BITCOIN, i['output_value']) for i in s.ins_bcy)
        v['outs'] = tuple( (o['value']/SATOSHIS_PER_BITCOIN, o['value']) for o in s.outs_bcy)
        v['fees'] = (s.txno_bcy['fees']/SATOSHIS_PER_BITCOIN, s.txno_bcy['fees'])
        return v


    ####################################
    ## SCRIPT TYPES (property)
    @property
    def script_types(s):
        """
        script types of the in an output scripts (from BlockCypher)
        """
        s._assert_bcy()
        t = {}
        t['ins']  = tuple( (i['script_type']) for i in s.ins_bcy )
        t['outs'] = tuple( (o['script_type']) for o in s.outs_bcy)
        return t


    ####################################
    ## RAWTXN (property)
    @property
    def rawtxn(s):
        """
        getter for the raw transaction
        """
        return s._rawtxn


    ####################################
    ## TXNO (property)
    @property
    def txno(s):
        """
        returns the transaction object
        """
        return s._txno

    ####################################
    ## TXNO_BEX (property)
    @property
    def txno_bex(s):
        """
        returns the transaction object as augmented by BlockExplorer
        """
        s._assert_bex()
        return s._txno_bex

    ####################################
    ## TXNO_BCY (property)
    @property
    def txno_bcy(s):
        """
        returns the transaction object as augmented by BlockCypher
        """
        s._assert_bcy()
        return s._txno_bcy


    ####################################
    ## INS (property)
    @property
    def ins(s):
        """
        returns the transaction inputs
        """
        return s._txno['ins']

    ####################################
    ## INS BEX (property)
    @property
    def ins_bex(s):
        """
        returns the transaction inputs, augmented by BlockExplorer
        """
        s._assert_bex()
        return s._txno_bex['vin']

    ####################################
    ## INS BCY (property)
    @property
    def ins_bcy(s):
        """
        returns the transaction inputs, augmented by BlockCypher
        """
        s._assert_bcy()
        return s._txno_bcy['inputs']


    ####################################
    ## OUTS (property)
    @property
    def outs(s):
        """
        returns the transaction outputs
        """
        return s._txno['outs']

    ####################################
    ## OUTS BEX (property)
    @property
    def outs_bex(s):
        """
        returns the transaction outputs, augmented by BlockExplorer
        """
        s._assert_bex()
        return s._txno_bex['vout']

    ####################################
    ## OUTS BCY (property)
    @property
    def outs_bcy(s):
        """
        returns the transaction outputs, augmented by BlockCypher
        """
        s._assert_bcy()
        return s._txno_bcy['outputs']


    ####################################
    ## OUTPUTS
    def outputs(s, index=None, source=True):
        """
        gets the output data of output `index`, source `source`

        NOTES:
        - if `index` is None, it returns a list of outputs

        - if `source` is True it returns a dict of outputs; if it is
          false'ish, only local data is used

        - a special source is 'lnk' which provides callables that
          retrieve the corresponding transaction object
        """

        if index is None:
            return [s.outputs(i, source) for i in range(len(s.outs))]

        if source is True:
            return { src:s.outputs(index, src) for src in ['raw', 'bex', 'bcy']}

        if not source: source = 'raw'
        if source==BLOCKEXPLORER: source = 'bex'
        if source==BLOCKCYPHER: source = 'bcy'

        try:
            if source == 'bcy': return s.outs_bcy[index]
            if source == 'bex': return s.outs_bex[index]
            if source == 'raw': return s.outs[index]
        except IndexError:
            raise WrongParameters("Outputs index out of range (index={}, max={})".format(index, len(s.outs)-1))

        raise WrongParameters("Unknown source (source='{}')".format(source))



    ####################################
    ## INPUTS
    def inputs(s, index=None, source=True):
        """
        gets the input data of input `index`, source `source`

        NOTES:
        - if `index` is None, it returns a list of inputs

        - if `source` is True it returns a dict of inputs; if it is
          false'ish, only local data is used

        - a special source is 'lnk' that provides callables that
          retrieve the corresponding transaction object (`txn`) and
          the object called out the correct output index (`outpoint`)
        """

        if index is None:
            return [s.inputs(i, source) for i in range(len(s.ins))]

        if source is True:
            return { src:s.inputs(index, src) for src in ['raw', 'bex', 'bcy', 'lnk']}

        if not source: source = 'raw'
        if source==BLOCKEXPLORER: source = 'bex'
        if source==BLOCKCYPHER: source = 'bcy'

        try:
            if source == 'bcy': return s.ins_bcy[index]
            if source == 'bex': return s.ins_bex[index]
            if source == 'raw': return s.ins[index]
            if source == 'lnk':

                if s._txno_bex is None and s._txno_bcy is None:
                    _assert_bcy()

                # get the hash of the previous transaction
                if not s._txno_bcy is None:
                    prev_txnid = s._txno_bcy['inputs'][index]['prev_hash']
                else:
                    prev_txnid = s._txno_bex['vin'][index]['txid']

                lnk = {}
                lnk['txn'] = lambda: s.__class__(txnhash=prev_txnid, network=s._network)
                lnk['outpoint'] = lambda: s.__class__(txnhash=prev_txnid, network=s._network, outix=index)
                return lnk

        except IndexError:
            raise WrongParameters("Inputs index out of range (index={}, max={})".format(index, len(s.ins)-1))

        raise WrongParameters("Unknown source (source='{}')".format(source))




    ####################################
    ## RETRIEVE RAWTXN
    @staticmethod
    def retrieve_rawtxn(txnhash, network):
        """
        retrieve the raw transation from the network (MAINNET or TESTNET)

        NOTE
        - this method uses blockexplorer via the the .nodes package

        - a derived class can overwrite it to use a different source
        """
        api = api_get(network==TESTNET, service=BLOCKEXPLORER)
        rawtxn = api.api_rawtx(txnhash)
        return rawtxn



    ####################################
    ## ASSERT BCY (private)
    def _assert_bcy(s):
        """
        assert BlockCypher information is present, or retrieve it
        """
        if not s._txno_bcy is None: return s
        api = api_get(s._network==TESTNET, service=BLOCKCYPHER)
        s._txno_bcy = api.api_txs(s.txnhash)
        return s


    ####################################
    ## ASSERT BEX (private)
    def _assert_bex(s):
        """
        assert BlockExplorer information is present, or retrieve it
        """
        if not s._txno_bex is None: return s
        api = api_get(s._network==TESTNET, service=BLOCKEXPLORER)
        s._txno_bex = api.api_tx(s.txnhash)
        return s
