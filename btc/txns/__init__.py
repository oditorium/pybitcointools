"""
Bitcoin transaction management


Defines an object representing a Bitcoin transaction. Implementation
code based on Vitalik Buterin's `pybitcointools` library.

The main submodule is `txns` from which all public symbols are imported.


(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""
__version__="0.1.2"

from .txns import *
from .analyze import *
