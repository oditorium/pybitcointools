"""
Bitcoin transaction management module, main component (do not import
directly)

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""
from ..const import *
from .impl import *
from ..keys import Key, addr

from collections import Iterable

import logging
logger = logging.getLogger(__name__)

######################################################################
## MODULE LEVEL METHODS

###################################
## transaction
def transaction(tx_or_tx_literal):
    """
    takes an address literal or object and returns an address
    """
    if isinstance(tx_or_tx_literal, str): return tx_or_tx_literal
    else: return tx_or_tx_literal.txn


####################################################################
## MAKE TRANSACTION (class)
class MakeTransaction():
    """
    builds a new transaction


    USAGE EXAMPLES

        #
        MakeTransaction.transfer_to(dest_addr, amount_sat).sign(my_key)

    """

    # default change address and fee if none is supplied
    # if it is None, the first signature will
    # (change in derived classes)
    DEFAULT_CHANGE_ADDR = None
    DEFAULT_FEE         = 500000     # btc.btcnts(0.5)


    ####################################
    ## CONSTRUCTOR
    def __init__(s, change_addr=None, fee=None, ins=None, outs=None, message=None):
        s._txn_obj = None

        s._ins  = []
        s.add_ins(ins)

        s._outs = []
        s.add_outs(outs)

        s.set_change_addr(change_addr)
        s.set_fee(fee)
        s.set_message(message)
        logger.debug("created transaction (ins={0.ins}, outs={0.outs}, ca={0.change_addr}, fee={0.fee}, msg={0.message})".format(s))


    ####################################
    ## TRANSFER TO (factory method)
    @classmethod
    def transfer_to(cls, dest_addr, amount_sat, change_addr=None, ins=None, fee=None, message=None):
        """
        creates a transfer for a single destination address (plus change)
        """
        logger.debug("transfer_to (dest_addr={}, amount_sat={}, change_addr={}, ins={}, fee={}, message={})".format(dest_addr, amount_sat, change_addr, ins, fee, message))
        return cls(
                    change_addr=change_addr,
                    fee = fee,
                    ins = ins,
                    outs = [{'value': amount_sat, 'address': addr(dest_addr)}],
                    message = message,
                )
    
    ####################################
    ## SWEEP TO (factory method)
    @classmethod
    def sweep_to(cls, dest_addr, ins, fee=None, message=None):
        """
        creates a sweep to a single destination address
        """
        logger.debug("ins = {}", ins)
        amount_sat = sum(x['value'] for x in ins)-fee
        return cls(
                    change_addr=None,
                    fee = fee,
                    ins = ins,
                    outs = [{'value': amount_sat, 'address': addr(dest_addr)}],
                    message = message,
                )

    ####################################
    ## COLLECT (factory method)
    @classmethod
    def collect(cls, ins=None, dest_addr=None, fee=None, message=None):
        """
        transfers all inputs (minus fees) to a single destination address

        NOTE
        - if dest_addr is not given, the default change address is
          used, and if it is None the address of the first signature
        """
        if not dest_addr is None: dest_addr = addr(dest_addr)
        return cls(
                    change_addr = dest_addr,
                    fee = fee,
                    ins = ins,
                    outs = [],
                    message = message,
                )

    ####################################
    ## FEE (property)
    @property
    def fee(s):
        """
        getter for the fee amount
        """
        return s._fee


    ####################################
    ## SET FEE
    def set_fee(s, fee=None):
        """
        setter for the fee (returns effective fee)

        NOTES
        - None means it is reset to the default fee

        - Returns the object so that methods can be chained
        """
        s._assert_not_locked()
        if fee is None:  s._fee = int(s.DEFAULT_FEE)
        else:            s._fee = int(fee)
        return s


    ####################################
    ## CHANGE ADDR (property)
    @property
    def change_addr(s):
        """
        getter for the change address amount
        """
        return s._change_addr


    ####################################
    ## SET CHANGE ADDR
    def set_change_addr(s, addr=None):
        """
        setter for the change address amount (returns effective address)

        NOTES
        - None means it is reset to the default address

        - Returns the object so that methods can be chained
        """
        s._assert_not_locked()
        if addr is None:  s._change_addr = s.DEFAULT_CHANGE_ADDR
        else:             s._change_addr = addr
        return s


    ####################################
    ## MESSAGE (property)
    @property
    def message(s):
        """
        getter for the OP_RETURN message included in the transaction
        """
        return s._opret_message


    ####################################
    ## SET MESSAGE
    def set_message(s, message=None):
        """
        setter for the OP_RETURN message included in the transaction

        NOTES
        - A new message replaces the previous one None means no message
          will be included

        - Returns the object so that methods can be chained
        """
        s._assert_not_locked()
        s._opret_message = message
        return s


    ####################################
    ## INS
    @property
    def ins(s):
        """
        getter for transaction inputs
        """
        return s._ins


    ####################################
    ## ADD INS
    def add_ins(s, input_or_inputs):
        """
        add a singles transaction input, or a list of inputs

        NOTE
        - Returns the object so that methods can be chained
        """

        # add None -> just return
        if input_or_inputs is None: return s._ins
        s._assert_not_locked()


        # make it into an iterable if there is only one
        if isinstance(input_or_inputs, dict): input_or_inputs = (input_or_inputs,)

        # append all items
        for inpt in input_or_inputs: s._ins.append(inpt)

        return s


    ####################################
    ## OUTS
    @property
    def outs(s):
        """
        getter for transaction outputs
        """
        return s._outs


    ####################################
    ## ADD OUTS
    def add_outs(s, output_or_outputs):
        """
        add a singles transaction output, or a list of outputs

        - Returns the object so that methods can be chained
        """

        # add None -> just return
        if output_or_outputs is None: return s._outs
        s._assert_not_locked()

        # make it into an iterable if there is only one
        if isinstance(output_or_outputs, dict): output_or_outputs = (output_or_outputs,)

        # append all items
        for outp in output_or_outputs: s._outs.append(outp)

        return s


    ####################################
    ## MAKE TRANSACTION
    def make_transaction(s, ignore_missing_change_addr=False):
        """
        creates a raw transaction from the inputs

        NOTES
        - it is not necessary to call this method; if `sign` is called without
          having called `make_transaction` then it is called automatically;
          in this case if `change_addr` had not been previously set, the address
          of the first signing key is used

        - from this point onwards the object is locked, and input paramters
          can no longer be changed

        - the lock can be removed calling `clear_transaction`

        - the function is idempotent: if it is called on a locked object
          it simply returns without changes

        - normally a missing change address will lead to a MissingChangeAddress
          exception being raised, unless `ignore_missing_change_addr` has been
          explicitly set true'ish

        - a transaction that is generated with a missing change address will be
          created as is, ignoring the fees; THIS CAN LEAD TO SUBSTANTIAL LOSSES
          AS THE DIFFERENCE IN INS AND OUTS WILL BE CONSIDERED MINING FEE BY
          THE BITCOIN NETWORK
        """

        # return without action if called again
        if not s._txn_obj is None: return

        # check whether change address is given
        if s.change_addr is None:

            if not ignore_missing_change_addr:
                raise MissingChangeAddress("cant make transaction (change address missing)")

            # create the transaction object from the raw data
            # NOTE THAT THIS CAN LEAD TO LARGE LOSSES
            s._txn_obj = make_transaction(s._ins, s._outs, serialized=False)

        else:

            # create the transaction object, including change and fee
            s._txn_obj =    make_send(
                                s._ins,
                                s._outs,
                                change_addr = s._change_addr,
                                fee = s._fee,
                                serialized=False
                            )

            # add opreturn message if requested
            if not s.message is None:
                s._txn_obj['outs'] += [opreturn_output(s.message)]

        return s


    ####################################
    ## CLEAR TRANSACTION
    def clear_transaction(s):
        """
        clear the current transaction and unlock the object (see `make_transaction`)
        """
        s._txn_obj = None
        return s



    ####################################
    ## TXNO (property)
    @property
    def txno(s):
        """
        return the transaction as Python object (or None)
        """
        return s._txn_obj


    ####################################
    ## TXN (property)
    @property
    def txn(s):
        """
        return the serialized transaction (or None)
        """
        if s._txn_obj is None: return None
        return serialize(s._txn_obj)


    ####################################
    ## signatures (property)
    @property
    def signatures(s):
        """
        returns list of bools (True means level is signed, false it is not)
        """
        if s._txn_obj is None: return None
        return [o['script']!="" for o in s.txno['ins']]


    ####################################
    ## sign
    def sign(s, key_or_key_literal, level=True, call_make_txn=True):
        """
        sign the input at a given level

        NOTE:

        - if level is True then all levels are signed with this particular
          key; level can also be a list

        - the key can be a key object or a literal; it must of course be
          (or contain) a private key

        - if `make_transaction` has not been previously called it will be
          called by sign; in case no change address has been given the
          change address of the first signing key is used
        """
        logger.debug("signing (key={}, level={})".format(key_or_key_literal, level))

        # after that, keyo will be key object
        if not isinstance(key_or_key_literal, Key):
            keyo = Key(key_or_key_literal)
        else: keyo = key_or_key_literal

        # if transaction has not been made, make it
        if s.txno is None:
            if s.change_addr is None: s.set_change_addr(keyo.addr)
            s.make_transaction()

        # if level is true then sign all levels that are yet unsigned
        if level is True:
            level = [lvl for lvl, signed in enumerate(s.signatures) if not signed]

        # otherwise if it is a single item, make it an iterable
        elif not isinstance(level, Iterable):
            level = (level,)

        # apply the signatures successively
        txn = s.txn
        for lvl in level:
            txn = sign(txn, lvl, keyo.priv(DEC))

        # remember the transaction object
        s._txn_obj = deserialize(txn)

        return s


    ####################################
    ## signed (property)
    @property
    def signed(s):
        """
        returns list of bools (True means level is signed, false it is not)
        """
        if s._txn_obj is None: return None
        return all(s.signatures)


    ####################################
    ## ASSERT NOT LOCKED (private)
    def _assert_not_locked(s):
        """
        asserts that the object is not locked yet, ie it still can be modified

        NOTES
        - the object gets locked once the transaction has been generated; after
          that a few things can no longer be changed (inputs, outputs, fee etc)

        - this method raises TransactionObjectLocked exception if locked
        """
        if not s._txn_obj is None:
            raise TransactionObjectLocked("the transaction object is locked and can no longer be modified")


    ####################################
    ## deserialize (static)
    @staticmethod
    def deserialize(txn):
        """
        deserialize a serialized transaction (static method)
        """
        return deserialize(txn)
