"""
Implementation functions for bitcoin transaction management

Usually you would not need to call those functions directly, they
are all wrapped within the `Transaction` class. They are based on
Vitalik Buterin's `pybitcointools` toolkit.

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""

from functools import reduce
from copy import deepcopy
from binascii import hexlify, unhexlify

from ..msgs.impl import ecdsa_raw_sign, ecdsa_raw_verify, ecdsa_raw_recover
from ..keys.impl import pubkey_from_privkey, privkey_encode, pubkey_encode, address_from_pubkey
from ..const import *
from ..technical import *

import logging
logger = logging.getLogger(__name__)



####################################################################
## HELPER FUNCTIONS

_STRING_TYPES = (str)
_STRING_OR_BYTES_TYPES = (str, bytes)
_INT_TYPES = (int, float)

####################################
## IS INP (private)
def _is_inp(arg_dict):
    """
    determines whether `arg_dict` is a transaction inputs
    """
    try:
        return len(arg_dict) > 64 or "output" in arg_dict or "outpoint" in arg_dict
    except TypeError:
        raise WrongParameters("wrong arg_dict format: '{}'".format(arg_dict))


####################################
## MAKE SCRIPT FOR PUBKEY
def make_script_for_pubkey(addr):
    """
    make an outputs script entry based on a public key (returns str)
    """
    # called mk_pubkey_script in Vitalik's code
    # Keep the auxiliary functions around for altcoins' sake
    return '76a914' + b58check_to_hex(addr) + '88ac'


####################################
## MAKE SCRIPT FOR ADDR
def make_script_for_addr(addr):
    """
    make an outputs script entry based on an address (returns str)
    """
    # called mk_scripthash_script in Vitalik's code

    return 'a914' + b58check_to_hex(addr) + '87'


####################################
## ADDRESS TO SCRIPT (private)
def address_to_script(addr):
    """
    make an outputs script entry based on an address or public key (returns str)
    """
    # called address_to_script in Vitalik's code

    if addr[0] == '3' or addr[0] == '2':  return make_script_for_addr(addr)
    else:                                 return make_script_for_pubkey(addr)




####################################################################
## SIGNATURE ENCODING

####################################
## DER ENCODE SIG
def der_encode_sig(v, r, s):
    """
    DER encode the signature
    """
    # called der_encode_sig in Vitalik's code

    b1, b2 = safe_hexlify(encode(r, 256)), safe_hexlify(encode(s, 256))
    if len(b1) and b1[0] in '89abcdef':   b1 = '00' + b1
    if len(b2) and b2[0] in '89abcdef':   b2 = '00' + b2
    left = '02'+encode(len(b1)//2, 16, 2)+b1
    right = '02'+encode(len(b2)//2, 16, 2)+b2
    return '30'+encode(len(left+right)//2, 16, 2)+left+right


####################################
## DER DECODE SIG
def der_decode_sig(sig):
    """
    DER decode the signature
    """
    # called der_decode_sig in Vitalik's code

    leftlen = decode(sig[6:8], 16)*2
    left = sig[8:8+leftlen]
    rightlen = decode(sig[10+leftlen:12+leftlen], 16)*2
    right = sig[12+leftlen:12+leftlen+rightlen]
    return (None, decode(left, 16), decode(right, 16))


####################################
## IS BIP66 (private)
def _is_bip66(sig):
    """
    Checks hex DER sig for BIP66 consistency
    """
    # called is_bip66 in Vitalik's code

    #https://raw.githubusercontent.com/bitcoin/bips/master/bip-0066.mediawiki
    #0x30  [total-len]  0x02  [R-len]  [R]  0x02  [S-len]  [S]  [sighash]
    sig = bytearray.fromhex(sig) if re.match('^[0-9a-fA-F]*$', sig) else bytearray(sig)
    if (sig[0] == 0x30) and (sig[1] == len(sig)-2):     # check if sighash is missing
            sig.extend(b"\1")		                   	# add SIGHASH_ALL for testing
    #assert (sig[-1] & 124 == 0) and (not not sig[-1]), "Bad SIGHASH value"

    if len(sig) < 9 or len(sig) > 73: return False
    if (sig[0] != 0x30): return False
    if (sig[1] != len(sig)-3): return False
    rlen = sig[3]
    if (5+rlen >= len(sig)): return False
    slen = sig[5+rlen]
    if (rlen + slen + 7 != len(sig)): return False
    if (sig[2] != 0x02): return False
    if (rlen == 0): return False
    if (sig[4] & 0x80): return False
    if (rlen > 1 and (sig[4] == 0x00) and not (sig[5] & 0x80)): return False
    if (sig[4+rlen] != 0x02): return False
    if (slen == 0): return False
    if (sig[rlen+6] & 0x80): return False
    if (slen > 1 and (sig[6+rlen] == 0x00) and not (sig[7+rlen] & 0x80)):
        return False
    return True


####################################
## SIGNATURE FORM (private)
def _signature_form(tx, i, script, hashcode=SIGHASH_ALL):
    """
    creates a transaction form ready to be signed (returns deep copy of tx object)
    """
    # called signature_form in Vitalik's code

    i, hashcode = int(i), int(hashcode)

    # if serialized -> deserialize, signature_form, reserialize
    if isinstance(tx, _STRING_OR_BYTES_TYPES):
        return serialize(_signature_form(deserialize(tx), i, script, hashcode))

    # create a new transaction object (via deep copy)
    newtx = deepcopy(tx)

    # reset all scripts in the new object to ""
    for inp in newtx["ins"]: inp["script"] = ""

    # set script at level `i` to `script`
    newtx["ins"][i]["script"] = script

    # SIGHASH_NONE -> reset outs to []
    if hashcode == SIGHASH_NONE:
        newtx["outs"] = []

    # SIGHASH_SINGLE -> TODO
    elif hashcode == SIGHASH_SINGLE:
        newtx["outs"] = newtx["outs"][:len(newtx["ins"])]
        for out in newtx["outs"][:len(newtx["ins"]) - 1]:
            out['value'] = 2**64 - 1
            out['script'] = ""

    # SIGHASH_SINGLE -> TODO
    elif hashcode == SIGHASH_ANYONECANPAY:
        newtx["ins"] = [newtx["ins"][i]]

    # ELSE (incl SIGHASH_ALL!) -> NOOP
    else: pass

    # return the new transaction
    return newtx




####################################################################
## SCRIPT SERIALISATION

####################################
## SCRIPT DESERIALIZE
def script_deserialize(script):
    """
    deserialize a script into (returns a list)

    EXAMPLE:

        script = make_script_for_pubkey("12AKRNHpFhDSBDD9rSn74VAzZSL3774PxQ")
        # -> 76a9140cbb3eeba6c4805531b18a1d27957fba561f868988ac
        script_ds = script_deserialize(script)
        # -> [118, 169, '5cbb3eeba6c4805531b18a1d27957fba561f8689', 136, 172]



    """
    # called deserialize_script in Vitalik's code

    if isinstance(script, str) and re.match('^[0-9a-fA-F]*$', script):
       return   json_change_base(
                    script_deserialize(binascii.unhexlify(script)),
                    lambda x: safe_hexlify(x)
                )

    out, pos = [], 0
    while pos < len(script):
        code = from_byte_to_int(script[pos])
        if code == 0:
            out.append(None)
            pos += 1
        elif code <= 75:
            out.append(script[pos+1:pos+1+code])
            pos += 1 + code
        elif code <= 78:
            szsz = pow(2, code - 76)
            sz = decode(script[pos+szsz: pos:-1], 256)
            out.append(script[pos + 1 + szsz:pos + 1 + szsz + sz])
            pos += 1 + szsz + sz
        elif code <= 96:
            out.append(code - 80)
            pos += 1
        else:
            out.append(code)
            pos += 1
    return out


####################################
## SCRIPT SERIALIZE UNIT
def script_serialize_unit(unit):
    """
    serialize a unit of a script
    """
    # called serialize_script_unit in Vitalik's code

    if isinstance(unit, int):
        if unit < 16:
            return from_int_to_byte(unit + 80)
        else:
            return from_int_to_byte(unit)

    elif unit is None:
        return b'\x00'

    else:
        if len(unit) <= 75:
            return from_int_to_byte(len(unit))+unit
        elif len(unit) < 256:
            return from_int_to_byte(76)+from_int_to_byte(len(unit))+unit
        elif len(unit) < 65536:
            return from_int_to_byte(77)+encode(len(unit), 256, 2)[::-1]+unit
        else:
            return from_int_to_byte(78)+encode(len(unit), 256, 4)[::-1]+unit


####################################
## SCRIPT SERIALIZE
def script_serialize(script):
    """
    serialize a script
    """
    # called serialize_script in Vitalik's code

    if json_is_base(script, 16):
        return  safe_hexlify(
                    script_serialize(
                        json_change_base(
                            script,
                            lambda x: binascii.unhexlify(x)
                )))

    result = bytes()
    for b in map(script_serialize_unit, script):
        result += b if isinstance(b, bytes) else bytes(b, 'utf-8')
    return result


####################################################################
## HEX TO BIN CONVERTER AND VICE VERSA FOR OBJECTS

####################################
## JSON IS BASE
def json_is_base(obj, base):
    """
    whether the json object is base `base`

    see also `json_change_base`
    """
    # called json_is_base in Vitalik's code

    # bytes -> never base `base` False
    if isinstance(obj, bytes): return False

    # code string 10 -> 0..9, 16 -> 0..9a..f etc
    alpha = get_code_string(base)

    # string -> returns False if string contains chars not in code string
    if isinstance(obj, str):
        for i in range(len(obj)):
            if alpha.find(obj[i]) == -1:
                return False
        return True

    # int or double or None -> always base `base`
    elif isinstance(obj, _INT_TYPES) or obj is None:
        return True

    # list -> True iff True for all elements
    elif isinstance(obj, list):
        for i in range(len(obj)):
            if not json_is_base(obj[i], base):
                return False
        return True

    # object -> True iff True for all elements (recursion)
    else:
        for x in obj:
            if not json_is_base(obj[x], base):
                return False
        return True


####################################
## JSON CHANGE BASE
def json_change_base(obj, changer):
    """
    change base of the json object using the `changer` function

    NOTES
    - if `changer` is `lambda x: binascii.unhexlify(x)` (or
      CHANGEBASE_FROM_HEX) then all strings and bytes objects are
      interpreted as hex, and converted appropriately; for example
      "4142" and b"4141" -> b"AB"

    - if `changer` is `lambda x: binascii.hexlify(x)` (or
      CHANGEBASE_TO_HEXB) then all bytes objects are converted into
      hex (fails on str); for example b"AB" -> b"4142", but "AB" ->
      error

    - if `safe_hexlify` is used instead of `binascii.hexlify` (or
      CHANGEBASE_TO_HEX) then the output will not be bytes but str

    - typically values are generated in hex to be in a human readable
      format, and unhexlify converts them into byte format; hexlify
      converts them back (into bytes representing hex, but simple
      decode() converts it into str)

    -
    """
    # called json_changebase in Vitalik's code

    # deal with presets
    if   changer == CHANGE_BASE_TO_HEX:   changer = lambda x: safe_hexlify(x)
    elif changer == CHANGE_BASE_TO_HEXB:  changer = lambda x: hexlify(x)
    elif changer == CHANGE_BASE_FROM_HEX: changer = lambda x: unhexlify(x)

    # string or bytes
    if isinstance(obj, _STRING_OR_BYTES_TYPES):
        return changer(obj)

    # int or double
    elif isinstance(obj, _INT_TYPES) or obj is None:
        return obj

    # list
    elif isinstance(obj, list):
        return [json_change_base(x, changer) for x in obj]

    # dict
    return dict((x, json_change_base(obj[x], changer)) for x in obj)



####################################################################
## MAIN INTERFACE FUNCTIONS

####################################
## DESERIALIZE
def deserialize(tx):
    """
    deserisalize a transaction (bytes format or str to Python object)
    """
    # called deserialize in Vitalik's code

    # string & hex characters only -> deserialise from hex
    if isinstance(tx, str) and re.match('^[0-9a-fA-F]*$', tx):
        #tx = bytes(bytearray.fromhex(tx))
        return json_change_base(
                    deserialize(binascii.unhexlify(tx)),
                    lambda x: safe_hexlify(x)
        )
    # http://stackoverflow.com/questions/4851463/python-closure-write-to-variable-in-parent-scope
    # Python's scoping rules are demented, requiring me to make pos an object
    # so that it is call-by-reference
    pos = [0]

    def read_as_int(bytez):
        pos[0] += bytez
        return decode(tx[pos[0]-bytez:pos[0]][::-1], 256)

    def read_var_int():
        pos[0] += 1

        val = from_byte_to_int(tx[pos[0]-1])
        if val < 253:
            return val
        return read_as_int(pow(2, val - 252))

    def read_bytes(bytez):
        pos[0] += bytez
        return tx[pos[0]-bytez:pos[0]]

    def read_var_string():
        size = read_var_int()
        return read_bytes(size)

    obj = {"ins": [], "outs": []}
    obj["version"] = read_as_int(4)
    ins = read_var_int()
    for i in range(ins):
        obj["ins"].append({
            "outpoint": {
                "hash": read_bytes(32)[::-1],
                "index": read_as_int(4)
            },
            "script": read_var_string(),
            "sequence": read_as_int(4)
        })
    outs = read_var_int()
    for i in range(outs):
        obj["outs"].append({
            "value": read_as_int(8),
            "script": read_var_string()
        })
    obj["locktime"] = read_as_int(4)
    return obj


####################################
## SERIALIZE
def serialize(txobj):
    """
    serisalize a transaction (Python object to bytes or str)

    Note: if the object is 'base 16' (not bytes; all strings valid
    hex characters) it can be serialised into a string, otherwise
    it is serialised into bytes
    """
    # called serialize in Vitalik's code

    # if it is a bytes object (why?) make it into a hex string
    if isinstance(txobj, bytes): txobj = bytes_to_hex_string(txobj)

    # simplified form if is base 16 -> str
    if json_is_base(txobj, 16):
        json_changedbase = json_change_base(txobj, lambda x: binascii.unhexlify(x))
        hexlified = safe_hexlify(serialize(json_changedbase))
        return hexlified

    # collect all parts in the list `o`
    o = []

    # version
    o.append(encode(txobj["version"], 256, 4)[::-1])

    # inputs
    o.append(num_to_var_int(len(txobj["ins"])))
    for inp in txobj["ins"]:
        o.append(inp["outpoint"]["hash"][::-1])
        o.append(encode(inp["outpoint"]["index"], 256, 4)[::-1])
        #o.append(num_to_var_int(len(inp["script"])) + from_string_to_bytes(inp["script"]))
        o.append(num_to_var_int(len(inp["script"]))+(inp["script"] if inp["script"] else bytes()))
        #o.append(num_to_var_int(len(inp["script"]))+(inp["script"] if inp["script"] or is_python2 else bytes()))
        o.append(encode(inp["sequence"], 256, 4)[::-1])

    # outputs
    o.append(num_to_var_int(len(txobj["outs"])))
    for out in txobj["outs"]:
        o.append(encode(out["value"], 256, 8)[::-1])
        #o.append(num_to_var_int(len(out["script"]))+from_string_to_bytes(out["script"]))
        o.append(num_to_var_int(len(out["script"]))+out["script"])

    # locktime
    o.append(encode(txobj["locktime"], 256, 4)[::-1])

    # concatenate the byte strings
    #return reduce(lambda x,y: x+from_string_to_bytes(y), o, bytes())
    return reduce(lambda x,y: x+y, o, bytes())


####################################
## MAKE TRANSACTION
def make_transaction(*args, serialized=True):
    """
    create a transaction (returns serialised object, unless `serialised`==False)

    NOTES:
     - input format is either [in0, in1...],[out0, out1...] or
       in0, in1 ... out0 out1 (or even mixed...)

    INPUTS:

        {
            'address':          '1CQLd3...',
            'block_height':     363957,
            'output':           'b0aad2e518...:0',
            'spend':            '0555181457...:0',
            'value':            500000,
        }

    OUTPUTS:

        {
            'value':            90000,
            'address':          '16iw1MQ1sy1DtRPYw3ao1bCamoyBJtRB4t'
        }

    """
    # called mktx i Vitalik's code
    #print(*args)


    # [in0, in1...],[out0, out1...] or in0, in1 ... out0 out1 ...

    # convert *args into the lists `ins` and `outs`
    ins, outs = [], []
    for arg in args:
        if isinstance(arg, list):
            for a in arg: (ins if _is_inp(a) else outs).append(a)
        else:
            (ins if _is_inp(arg) else outs).append(arg)

    # initialise transaction object
    txobj = {"locktime": 0, "version": 1, "ins": [], "outs": []}

    # add all the ins
    for i in ins:
        if isinstance(i, dict) and "outpoint" in i:
            txobj["ins"].append(i)
        else:
            if isinstance(i, dict) and "output" in i:
                i = i["output"]
            txobj["ins"].append({
                "outpoint": {"hash": i[:64], "index": int(i[65:])},
                "script": "",
                "sequence": 4294967295
            })

    # add all the outs
    for o in outs:

        if isinstance(o, str):
            # string address of type '<address/script>:<value>'
            # --> overwrite `o` with a dict
            addr = o[:o.find(':')]
            val = int(o[o.find(':')+1:])
            o = {}
            if re.match('^[0-9a-fA-F]*$', addr):
                # script output
                o["script"] = addr

            else:
                # address output
                o["address"] = addr

            o["value"] = val

        # now o is an object, even if it was a string before
        outobj = {}
        if "address" in o:
            # convert addresses to script before appending them
            outobj["script"] = address_to_script(o["address"])

        elif "script" in o:
            # scripts are appended directly
            outobj["script"] = o["script"]

        else:
            raise WrongOutputsFormat("Could not find 'address' or 'script' in output.")

        outobj["value"] = o["value"]
        txobj["outs"].append(outobj)

    # return the object, usually serialized
    if serialized: return serialize(txobj)
    else: return txobj

####################################
## OPRETURN OUTPUT
def opreturn_output(message):
    """
    creates an OP_RETURN output containing `message`
    """
    # does not exist in Vitalik's code

    # script = OP_RETURN + length byte + message
    OP_RETURN = b"\x6a"
    message_bin = message.encode()
    len_byte = bytes([len(message_bin)])
    script_bin = OP_RETURN+len_byte+message_bin
    return {'value': 0, 'script': bytes_to_hex_string(script_bin)}


####################################
## SELECT INPUTS
def select_inputs(unspent, value, raise_if_too_low=True):
    """
    select the transaction inputs needed to come to a given value

    NOTE: `unspent` is a list of dicts, each of which represent one
    unspent transaction input; value is the target value in Satoshi;
    if `raise_if_too_low` is True, insufficient funds raises an
    exception (otherwise this would return False)
    """
    # called select in Vitalik's code

    # convert value to int
    value = int(value)

    # high is list of all utxo bigger than value
    high = [u for u in unspent if u["value"] >= value]
    high.sort(key=lambda u: u["value"])
    if len(high): return [high[0]] # take the smallest one

    # low is the list of all utxo smaller than value
    low = [u for u in unspent if u["value"] < value]
    low.sort(key=lambda u: -u["value"])

    # sum all utxo until either exhausted or value achieved
    i, tv = 0, 0
    while tv < value and i < len(low):
        tv += low[i]["value"]
        i += 1

    # check whether there are sufficient funds
    if tv < value:
        if raise_if_too_low:
            raise InsufficientFunds("Not enough funds (have={}, need={})".format(tv, value))
        else: return False

    # return the list of selected transaction inputs
    return low[:i]


####################################
## MAKE SEND
def make_send(*ins_outs, change_addr=None, fee=None, serialized=True):
    """
    creates a transaction that includes a fee and change

    Note
    - always raises an exception if there are insufficient funds, or if
      there is no change address provided

    - the parameters change_addr, fee, and serialized must ALWAYS
      be supplied as keyword arguments, for example

            txn.make_send(ins, outs, change_addr=myaddr, myfee=f)
    """
    # called mksend in Vitalik's code

    # Only takes inputs of the form { "output": blah, "value": foo }

    # separate in ins_outs, the change address, and the fee amount
    #ins_outs, change, fee = args[:-2], args[-2], int(args[-1])
    if fee is None: fee = 0

    # distribute inputs and outputs into ins and outs respectively
    ins, outs = [], []
    for arg in ins_outs:
        if isinstance(arg, list):
            for a in arg:
                (ins if _is_inp(a) else outs).append(a)
        else:
            (ins if _is_inp(arg) else outs).append(arg)

    # isum is the value of all inputs
    isum = sum([i["value"] for i in ins])
    #print ("Sum of inputs  = {:9}".format(isum))

    # osum is the value of all outputs, outputs2 a new list of outputs
    osum, outputs2 = 0, []
    for o in outs:
        # convert string into dicts
        if isinstance(o, str):
            o2 = {
                "address": o[:o.find(':')],
                "value": int(o[o.find(':')+1:])
            }
        else: o2 = o
        outputs2.append(o2)
        osum += o2["value"]
    #print ("Sum of outputs = {:9}".format(osum))

    # check whether there are sufficient funds
    if isum < osum+fee:
        raise InsufficientFunds("Not enough funds (have={}, need={})".format(isum, osum+fee))

    # check whether it is worth sending the change; if yes: add and entry
    elif isum > osum+fee+5430:
        if change_addr is None:
            raise MissingChangeAddress("No change address provided (amount = {})".format(isum-osum-fee))
        outputs2 += [{"address": change_addr, "value": isum-osum-fee}]

    #return (outputs2)

    # make a transaction record
    return make_transaction(ins, outputs2, serialized=serialized)


####################################
## TRANSACTION HASH HEX
def transaction_hash_hex(tx, hashcode=None):
    """
    calculates the transaction hash (output in hex format)
    """
    # called txhash in Vitalik's code
    if isinstance(tx, str) and re.match('^[0-9a-fA-F]*$', tx):
        tx = change_base(tx, 16, 256)
    if hashcode:
        return dbl_sha256_hex(
            from_string_to_bytes(tx) + encode(int(hashcode), 256, 4)[::-1])
    else:
        return safe_hexlify(bin_dbl_sha256_hex(tx)[::-1])


####################################
## TRANSACTION HASH BIN
def transaction_hash_bin(tx, hashcode=None):
    """
    calculates the transaction hash (output in bytes)
    """
    # called bin_txhash in Vitalik's code

    return binascii.unhexlify(transaction_hash_hex(tx, hashcode))


####################################
## TRANSACTION ECDSA SIGN
def transaction_ecdsa_sign(tx, priv_dec, hashcode=SIGHASH_ALL):
    """
    signs a transaction using the ECDSA signature algorithm (returns signature)
    """
    # called ecdsa_tx_sign in Vitalik's code

    rawsig = ecdsa_raw_sign(transaction_hash_bin(tx, hashcode), priv_dec)
    return der_encode_sig(*rawsig)+encode(hashcode, 16, 2)


####################################
## TRANSACTION ECDSA VERIFY
def transaction_ecdsa_verify(tx, sig_hex, pub_dec, hashcode=SIGHASH_ALL):
    """
    verifies a transaction signed with the ECDSA signature algorithm

    Note:
    - the signature is in hex format, the way it has been generated
      by `transaction_ecdsa_sign`

    - the transaction is also in hex format (or possibly bytes?), the
      way it has been generated eg by `make_transaction`

    - the public key is in DEC format

    - it only works when `ecdsa_raw_recover` ignores that the v value
      is not in its usual range
    """
    # called ecdsa_tx_verify in Vitalik's code

    return ecdsa_raw_verify(
                transaction_hash_bin(tx, hashcode),
                der_decode_sig(sig_hex),
                pub_dec,
                ignore_v_error=True
            )


####################################
## TRANSACTION ECDSA RECOVER
def transaction_ecdsa_recover(tx, sig_hex, hashcode=SIGHASH_ALL):
    """
    recovers public keys from a signed transaction

    Note:
    - the signature is in hex format, the way it has been generated
      by `transaction_ecdsa_sign`

    - the transaction is also in hex format (or possibly bytes?), the
      way it has been generated eg by `make_transaction`

    - it only works when `ecdsa_raw_recover` ignores that the v value
      is not in its usual range

    - it returns two public keys, encoded in hex format; apparently the
      second key returned corresponds to the public key used to generate
      the signature(?)
    """
    # called ecdsa_tx_recover in Vitalik's code

    z = transaction_hash_bin(tx, hashcode)
    _, r, s = der_decode_sig(sig_hex)
    left  = ecdsa_raw_recover(z, (0, r, s), True)
    right = ecdsa_raw_recover(z, (1, r, s), True)
    return (pubkey_encode(left, HEX), pubkey_encode(right, HEX))


####################################
## TRANSACTION INPUT VERIFY
def transaction_input_verify(tx, i, script, sig, pub):
    """
    verify the transaction input at level i
    """
    # called verify_tx_input in Vitalik's code

    # convert transaction to bytes if hexlified
    if isinstance(tx, str):
        if re.match('^[0-9a-fA-F]*$', tx):      tx = binascii.unhexlify(tx)

    # convert script to bytes if hexlified
    if isinstance(script, str):
        if re.match('^[0-9a-fA-F]*$', script):  script = binascii.unhexlify(script)

    # hexlifiy the signature if not hexlified already
    if isinstance(script, bytes) or not re.match('^[0-9a-fA-F]*$', sig):
        sig = safe_hexlify(sig)

    # prepare parameters
    hashcode = decode(sig[-2:], 16)
    modtx = _signature_form(tx, int(i), script, hashcode)

    # verify the signature
    return ecdsa_tx_verify(modtx, sig, pub, hashcode)


####################################
## SIGN
def sign(tx, i, priv_dec, hashcode=SIGHASH_ALL):
    """
    sign the transaction object at level `i` (returns the signed object)

    NOTE: the key must be in DEC format
    """
    # called sign in Vitalik's code

    # transaction level i must be an int
    i = int(i)
    #if (not is_python2 and isinstance(re, bytes)) or not re.match('^[0-9a-fA-F]*$', tx):

    # something something transaction format
    if isinstance(re, bytes) or not re.match('^[0-9a-fA-F]*$', tx):
        return binascii.unhexlify(sign(safe_hexlify(tx), i, priv))

    #if len(priv) <= 33: priv = safe_hexlify(priv)

    # generate public key and address corresponding to private key
    priv_bin = privkey_encode(priv_dec, BIN)
    pub_dec  = pubkey_from_privkey(priv_dec)
    pub_bin  = pubkey_encode(pub_dec, BIN)
    pub_hex  = pubkey_encode(pub_dec, HEX)
    address  = address_from_pubkey(pub_bin)

    # get the transaction
    signing_tx = _signature_form(tx, i, make_script_for_pubkey(address), hashcode)

    # generate the signature
    sig = transaction_ecdsa_sign(signing_tx, priv_dec, hashcode)

    # deserialise the object, insert the signature, and reserialise it
    txobj = deserialize(tx)
    txobj["ins"][i]["script"] = script_serialize([sig, pub_hex])
    return serialize(txobj)


####################################
## SIGN ALL
def sign_all(tx, priv_key_dec_or_key_dict):
    """
    sign all items in a transaction (either with same key, or dict of keys)

    Note:
    - if priv_key_dec_or_key_dict is a dictionary, the format
      must be { 'txinhash:txinidx' : privkey }

    - the private key (or keys, in the dict) must be in DEC format

    """
    # called signall in Vitalik's code


    if isinstance(priv_key_dec_or_key_dict, dict):

        # dict of multiple keys
        # -> note: (indexed 'txhash:txidx')
        key_dict = priv_key_dec_or_key_dict
        for e, i in enumerate(deserialize(tx)["ins"]):
            hazh    = i["outpoint"]["hash"]
            ix      = i["outpoint"]["index"]
            key_dec = key_dict["%s:%d" % (hazh, ix)]
            tx      = sign(tx, e, key_dec)

    else:

        # sign all items with the same single key
        priv_dec = priv_key_dec_or_key_dict
        for i in range(len(deserialize(tx)["ins"])):
            tx = sign(tx, i, priv_dec)

    return tx

####################################
## SIGN ALL
