"""
Bitcoin tools

Container module for various bitcoin-related modules. It is mostly
based on Vitalik Buterin's `pybitcointools` repo but is providing
a more structured API to the underlying functions, and the code
has been refactored and annotated.

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""
__version__         = "0.1.1"
__version_dt__      = "August 2017"
__author__          = "Stefan LOESCH"
__copyright__       = "(c) Copyright Stefan LOESCH 2017."
__license__         = "MPL v2.0"

from .const import *
from . import keys
from . import msgs
from . import txns
from . import nodes
