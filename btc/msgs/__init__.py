"""
Bitcoin message management


Defines an object representing a message that can be signed using
Bicoin keyks. Implementation  code based on Vitalik Buterin's
`pybitcointools` library.

The main submodule is `msgs` from which all public symbols are imported.


(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""
__version__="0.2"

from .msgs import *
