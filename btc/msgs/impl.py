"""
Implementation functions for bitcoin message management

Usually you would not need to call those functions directly, they
are all wrapped within the `Message` class. They are based on Vitalik
Buterin's `pybitcointools` toolkit.

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""

import base64
import hmac

from ..keys.impl import privkey_encode
from ..technical import *
from ..const import *

import logging
logger = logging.getLogger(__name__)


####################################################################
## HELPER FUNCTIONS

####################################
## SIG ENCODE (private)
def _sig_encode(v, r, s):
    """
    encode a signature
    """
    # called encode_sig in Vitalik's code

    vb, rb, sb = from_int_to_byte(v), encode(r, 256), encode(s, 256)

    result = base64.b64encode(vb+b'\x00'*(32-len(rb))+rb+b'\x00'*(32-len(sb))+sb)
    return str(result, 'utf-8')


####################################
## SIG DECODE (private)
def _sig_decode(sig):
    """
    decode a signature
    """
    # called decode_sig in Vitalik's code

    bytez = base64.b64decode(sig)
    return from_byte_to_int(bytez[0]), decode(bytez[1:33], 256), decode(bytez[33:], 256)


####################################
## DETERMINISTIC GEN K (private)
def deterministic_generate_k(msghash, priv_bin):
    """
    generate K deterministically

    Note: the private key `priv_bin` must be in the BIN format
    """
    # called deterministic_gen_k in Vitalik's code

    v = b'\x01' * 32
    k = b'\x00' * 32
    #priv = privkey_encode(priv, BIN)
    msghash = encode(hash_to_int(msghash), 256, 32)
    k = hmac.new(k, v+b'\x00'+priv_bin+msghash, hashlib.sha256).digest()
    v = hmac.new(k, v, hashlib.sha256).digest()
    k = hmac.new(k, v+b'\x01'+priv_bin+msghash, hashlib.sha256).digest()
    v = hmac.new(k, v, hashlib.sha256).digest()
    return decode(hmac.new(k, v, hashlib.sha256).digest(), 256)




####################################################################
## RAW SIGNATURE FUNCTIONS

####################################
## ECDSA RAW SIGN
def ecdsa_raw_sign(msghash, priv_dec):
    """
    generate a raw ECDSA signature on the hash(!) of the message

    Note: the private key `priv_dec` must be in DEC format
    """
    # called ecdsa_raw_sign in Vitalik's code

    # encode the DEC key in BIN format
    priv_bin = privkey_encode(priv_dec, BIN)

    z = hash_to_int(msghash)
    k = deterministic_generate_k(msghash, priv_bin)

    r, y = fast_multiply(kG, k)
    #s = inv(k, kN) * (z + r*decode_privkey(priv)) % kN
    s = inv(k, kN) * (z + r*priv_dec) % kN

    v, r, s = 27+((y % 2) ^ (0 if s * 2 < kN else 1)), r, s if s * 2 < kN else kN - s
    #if 'compressed' in get_privkey_format(priv): v += 4
    return v, r, s


####################################
## ECDSA RAW VERIFY
def ecdsa_raw_verify(msghash, vrs, pub_dec, ignore_v_error=False):
    """
    verify a raw ECDSA signature

    Note
    - the public key `pub_dec` must be in DEC format
    - if `ignore_v_error` is True then the range of v is not verified
    """
    # called ecdsa_raw_verify in Vitalik's code

    v, r, s = vrs

    if not ignore_v_error:
        if not (27 <= v <= 34):
            return False

    w = inv(s, kN)
    z = hash_to_int(msghash)

    u1, u2 = z*w % kN, r*w % kN
    #x, y = fast_add(fast_multiply(kG, u1), fast_multiply(decode_pubkey(pub), u2))
    x, y = fast_add(fast_multiply(kG, u1), fast_multiply(pub_dec, u2))
    return bool(r == x and (r % kN) and (s % kN))



####################################
## ECDSA RAW RECOVER
def ecdsa_raw_recover(msghash, vrs, ignore_v_error=False):
    """
    recover a public key from message hash and v, r, s (returns a DEC key)

    Note
    - vrs is a tuple (v,r,s) = _sig_decode(signature) in messaging; however,
      upon decode v is always None!
    - if `ignore_v_error` is True then the range of v is not verified
    """
    # called ecdsa_raw_recover in Vitalik's code

    v, r, s = vrs
    if not ignore_v_error:
        if not (27 <= v <= 34):
            raise ValueError("%d must in range 27-31" % v)
    x = r
    xcubedaxb = (x*x*x+kA*x+kB) % kP
    beta = pow(xcubedaxb, (kP+1)//4, kP)
    y = beta if v % 2 ^ beta % 2 else (kP - beta)
    # If xcubedaxb is not a quadratic residue, then r cannot be the x coord
    # for a point on the curve, and so the sig is invalid
    if (xcubedaxb - y*y) % kP != 0 or not (r % kN) or not (s % kN):
        return False
    z  = hash_to_int(msghash)
    Gz = jacobian_multiply((kGx, kGy, 1), (kN - z) % kN)
    XY = jacobian_multiply((x, y, 1), s)
    Qr = jacobian_add(Gz, XY)
    Q  = jacobian_multiply(Qr, inv(r, kN))
    Q  = from_jacobian(Q)

    # if ecdsa_raw_verify(msghash, vrs, Q):
    return Q
    # return False


####################################
## ELECTRUM SIG HASH (private)
def electrum_sig_hash(message):
    """
    compute a message hash based on the Electrum algo

    algo is double-SHA256 of prefix + length + message
    """
    # called electrum_sig_hash in Vitalik's code

    # WTF, Electrum?
    padded = b"\x18Bitcoin Signed Message:\n" + \
        num_to_var_int(len(message)) + from_string_to_bytes(message)
    return bin_dbl_sha256_hex(padded)





####################################################################
## SIGNATURE FUNCTIONS

####################################
## ECDSA SIGN HASH
def ecdsa_sign_hash(message_hash, priv_dec):
    """
    generate an ECDSA signature of `message_hash` with private key

    Note:
        - the private key `priv_dec` must be in DEC format
        - the private key `priv_bin` must be in BIN format
        - `message_hash` will usually be `electrum_sig_hash(message)`
        - there is no check whether they are the same
    """
    # called ecdsa_sign in Vitalik's code


    #v, r, s = _ecdsa_raw_sign(electrum_sig_hash(message), priv_dec, priv_bin)
    v, r, s = ecdsa_raw_sign(message_hash, priv_dec)
    sig = _sig_encode(v, r, s)
    #assert ecdsa_verify(message, sig, privtopub(priv)), "Bad Sig!\t %s\nv = %d\n,r = %d\ns = %d" % (sig, v, r, s)
    return sig


####################################
## ECDSA VERIFY HASH PUB
def ecdsa_verify_hash_pub(message_hash, signature, pub):
    """
    verify ECDSA signature that `signature` for `message_hash` corresponds to `pub` key

    Note:
        - `message_hash` will usually be `electrum_sig_hash(message)`
        - `pub` key must be in TBC format
    """
    # called ecdsa_verify in Vitalik's code

    #if is_address(pub): return ecdsa_verify_addr(message, signature, pub)
    #return ecdsa_raw_verify(electrum_sig_hash(message), _sig_decode(signature), pub)
    return ecdsa_raw_verify(message_hash, _sig_decode(signature), pub)


####################################
## ECDSA VERIFY ADDR
def _ecdsa_verify_hash_addr(message_hash, sig, addr):
    """
    verify ECDSA signature that `signature` for `message_hash` corresponds to `addr` address

    Note:
        - `message_hash` will usually be `electrum_sig_hash(message)`
    """
    # called ecdsa_verify_addr in Vitalik's code

    raise NotImplementedError("must use ecdsa_recover_key and ecdsa_verify_hash_pub")
    # For BitcoinCore, (msg = addr or msg = "") be default
    #assert is_address(addr)
    Q = _ecdsa_recover(message_hash, sig)
    magic = get_version_byte(addr)
    return (addr == pubtoaddr(Q, int(magic))) or (addr == pubtoaddr(compress(Q), int(magic)))


####################################
## ECDSA RECOVER
def ecdsa_recover(message_hash, signature):
    """
    recover a key from `message` and `signature`

    Note:
        - `message_hash` will usually be `electrum_sig_hash(message)`
    """
    # called ecdsa_recover in Vitalik's code

    v,r,s = _sig_decode(signature)
    #Q = ecdsa_raw_recover(electrum_sig_hash(message), (v,r,s))
    Q = ecdsa_raw_recover(message_hash, (v,r,s))
    return Q
    #return encode_pubkey(Q, 'hex_compressed') if v >= 31 else encode_pubkey(Q, 'hex')
