"""
Bitcoin message management module, main component (do not import
directly)

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""

from ..const import *
from .impl import *
from ..keys import Key
from ..keys.impl import version_byte_from_addr, address_from_pubkey

from collections import namedtuple

import logging
logger = logging.getLogger(__name__)



####################################################################
## CLASS MESSAGE
class Message():
    """
    represents a signed message, and can be used to sign messages

    USAGE

        # computing a message hash
        hsh = Message(my_message).message_hash
        hsh = Message.new_message(my_message).message_hash

        # creating a message signature
        signature = Message(my_message, my_private_key).signature
        signature = Message.new_message(my_message, my_private_key).signature

        # signing a message and retrieving the signature and address (as namedtuple)
        full_signature = Message(my_message, my_private_key).full_signature

        # signing a message and retrieving the full signed message (as namedtuple)
        signed_message = Message(my_message, my_private_key).signed_message

        # verifying a message signature
        verified = Message.load_message(my_message, signature, sig_addr_or_key).verify()

        # asserting a message is properly signed (raises SignatureFailedVerification if not)
        Message.load_message(my_message, the_signature, sig_addr_or_key).verify(raise_if_invalid=True)

    """

    ####################################
    ## HASH_FUNCTION (private static)
    @staticmethod
    def _hash_function(message):
        """
        the hash function used to hash a message (derived classes can overwrite)
        """
        return electrum_sig_hash(message)


    ####################################
    ## CONSTRUCTOR
    def __init__(s, message, signature=None):
        """
        creates a message object, possibly providing signature to be verified
        """

        # store message, hash, and signature
        s._message        = message
        s._message_hash   = s._hash_function(message)
        s._signature      = signature

    ####################################
    ## SIGN
    def sign(s, private_key_or_key_literal):
        """
        sign the message with the `key` (can be object or literal)
        """
        # convert key into object if it is a key literal
        key = Key(private_key_or_key_literal)

        # create the signature, and save it as well as the key address
        s._signature = \
            ecdsa_sign_hash(s._message_hash, key.priv(DEC))
        s._signature_addr = key.addr
        return s


    ####################################
    ## VERIFY
    def verify(s, signature_addr_or_key, signature=None, raise_if_invalid=False):
        """
        verify signature (returns True if verified, False else)
        """

        if isinstance(signature_addr_or_key, Key):
            s._signature_addr = signature_addr_or_key.addr
        else:
            s._signature_addr = str(signature_addr_or_key)

        if not signature is None:
            s._signature = signature

        # check we have everything we need
        if s._signature is None: raise WrongParameters("Signature is missing")
        if s._signature_addr is None: raise WrongParameters("Address is missing")

        # verify the signature, and raise if requested and necessary
        try:
            # Vitalik's code raises a lot of exceptions if parameters are wrong....
            recovered_key = ecdsa_recover(s.message_hash, s._signature) # yields a DEC key
            recovered_key = Key(recovered_key)
            magic = version_byte_from_addr(s._signature_addr)
            verified = (s._signature_addr ==
                    address_from_pubkey(recovered_key.publ(BIN), int(magic)) )
        except:
            verified = False

        # raise if requested and necessary
        if not raise_if_invalid: return verified
        if not verified:
            raise SignatureFailedVerification(
                "Signature failed verification (hash={}, sig={}, addr={})".format(s.message_hash, s._signature, s._signature_addr))
        return verified


    ####################################
    ## MESSAGE (property)
    @property
    def message(s):
        """
        getter for the message that is being signed
        """
        return s._message

    ####################################
    ## MESSAGE_HASH (property)
    @property
    def message_hash(s):
        """
        the hash of the message (using Electrum algo)
        """
        return s._message_hash

    ####################################
    ## SIGNATURE (property)
    @property
    def signature(s):
        """
        the signature of the message
        """
        return s._signature

    ####################################
    ## SIGNATURE ADDR (property)
    @property
    def signature_addr(s):
        """
        the address corresponding to the signature key
        """
        return s._signature_addr

    ####################################
    ## FULL SIGNATURE (property)
    @property
    def full_signature(s):
        """
        the signature and the address corresponding to the signature key (named tuple)
        """
        return namedtuple('Signature', ['signature', 'address'])(s.signature, s.signature_addr)

    ####################################
    ## SIGNED MESSAGE (property)
    @property
    def signed_message(s):
        """
        message, hash, and full signature (named tuple)
        """
        return namedtuple('SignedMessage', ['message', 'hash', 'signature', 'address'])(
                                     s.message, s.message_hash, s.signature, s.signature_addr)

    ####################################
    ## IS SIGNED (property)
    @property
    def is_signed(s):
        """
        whether the message has been signed
        """
        return not s.signature is None
