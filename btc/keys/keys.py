"""
Bitcoin key management module, main component (do not import directly)

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>

ISSUES
    - the compressed binary format does not seem to work for public keys

"""
import random
import time
from collections import OrderedDict, Iterable
import re

from ..const import *
from . import impl

import logging
logger = logging.getLogger(__name__)


######################################################################
## MODULE LEVEL METHODS

###################################
## KEYFORMAT
def keyformat(key_literal, human_readable=True, as_str=True):
    """
    format name and type of a given key literal

    NOTES
    - if as_str is false'ish the function returns a tuple, otherwise
      this tuple is concatenated into a string (only human readable)

    - if human_readable is true'ish the respective names are used,
      otherwise the numbers correspondig to the constants
    """
    if not human_readable: as_str=False

    if impl.is_privkey(key_literal):
        ktype = PRIV
        kformt = impl.privkey_get_format(key_literal)

    elif impl.is_pubkey(key_literal):
        ktype = PUB
        kformt = impl.pubkey_get_format(key_literal)

    elif impl.is_addr(key_literal):
        ktype = ADDR
        kformt = ADDRF

    else:
        raise WrongKeyFormat("Key is not in a recognised format {}".format(key_literal))

    if human_readable:
        ktype  = KEY_TYPE_NAMES[ktype]
        kformt = KEY_FORMAT_NAMES[kformt]

    if not as_str: return ktype, kformt
    return "{}:{}".format(ktype, kformt)

###################################
## IS PRIVKEY (static)
def is_privkey(key_literal):
    """
    true iff the key literal represents a private key
    """
    return impl.is_privkey(key_literal)

###################################
## IS PUBKEY
def is_pubkey(key_literal):
    """
    true iff the key literal represents a public key
    """
    return impl.is_pubkey(key_literal)

###################################
## IS ADDR
@staticmethod
def is_addr(key_literal):
    """
    true iff the key literal represents an address
    """
    return impl.is_addr(key_literal)

###################################
## addr
def addr(key_or_addr_literal):
    """
    takes an address or a key and returns an address
    """
    if isinstance(key_or_addr_literal, str): return key_or_addr_literal
    elif isinstance(key_or_addr_literal, Key): return key_or_addr_literal.addr
    else:
        raise WrongKeyFormat("can't coerce input into address (inp={})".format(key_or_addr_literal))

######################################################################
## KEY (class)
class Key():
    """
    represents a bitcoin key (address, or addr & public, or addr & publ & private)
    """

    ###################################
    ## KEY FORMATS
    DECIMAL         = DECIMAL           # integer
    DEC             = DEC
    BIN             = BIN               # byte string
    BIN_COMPRESSED  = BIN_COMPRESSED
    BINC            = BINC
    HEX             = HEX               # hex string
    HEX_COMPRESSED  = HEX_COMPRESSED
    HEXC            = HEXC
    WIF             = WIF               # base 58 with check sum
    WIF_COMPRESSED  = WIF_COMPRESSED
    WIFC            = WIFC
    ELECTRUM_BIN    = ELECTRUM_BIN      # electrum byte string
    EBIN            = EBIN
    ELECTRUM_HEX    = ELECTRUM_HEX      # electrum  hex string
    EHEX            = EHEX

    ##########################################################
    # KEY TYPES
    ADDR = ADDRESS
    PUB  = PUBLIC
    PRIV = PRIVATE

    ##########################################################
    # Network
    MAINNET = MAINNET
    BITCOIN = BITCOIN
    TESTNET = TESTNET


    ###################################
    ## FORMAT NAMES (private)
    _FORMAT_NAMES = KEY_FORMAT_NAMES

    ###################################
    ## TYPE NAMES (private)
    _TYPE_NAMES = KEY_TYPE_NAMES

    ###################################
    ## NETWORK NAMES (private)
    _NETWORK_NAMES = NETWORK_NAMES


    ###################################
    ## CONSTRUCTOR
    def __init__(s, key_or_key_literal=None, testnet=None):
        """
        constructor
        """
        if testnet is None: testnet = False
        s._network = MAINNET if not testnet else TESTNET

        if key_or_key_literal is None: key_or_key_literal = s.random_key
        s._init_from_key_or_key_literal(key_or_key_literal)


    ###################################
    ## FROM PASSPHRASE (factory)
    @classmethod
    def from_passphrase(cls, *args, testnet=None):
        """
        create private key from passphrase ("brain wallet")

        NOTES
        - this is a factory method, ie a class method that returns
          an instance

        - args are converted to string and concatenated with ":"

        - if the pass phrase starts with eg "25x:" then this is an
          indication that the part after the : should be hashed
          repeatedly (here: 25x100 times)


        TIMING (Mac)

            Phrase          Time        Repetitions
            ======          =====       ===========
            "pp"            0.2ms       1
            "1x:pp"         2.5ms       100
            "5x:pp"         15ms        500
            "10x:pp"        30ms        1000
        """
        passphrase = ":".join(map(str, args))
        match = re.match("([0-9]*)x:(.*)", passphrase)
        if not match is None:
            iterations = int(match.groups()[0])*100
            passphrase = match.groups()[1]
        else:
            iterations = 1
        #print (passphrase)

        # run the itrations
        privkey = passphrase
        for _ in range(iterations):
            privkey = impl.sha256_hex(privkey)

        return cls(privkey, testnet)


    ###################################
    ## RANDOM KEY (property)
    @property
    def random_key (s):
        """
        generates a random private key
        """
        entropy = impl.random_string(32) \
            + str(random.randrange(2**256)) \
            + str(int(time.time() * 1000000))
        privkey = impl.sha256_hex(entropy)
        return privkey


    ###################################
    ## INIT FROM LITERAL (private)
    def _init_from_key_or_key_literal(s, key_or_key_literal):
        """
        initialise from key literal (private or public)
        """

        if isinstance(key_or_key_literal, s.__class__):

            # initialise from key object
            keytype = key_or_key_literal.keytype
            if keytype==PRIVATE:
                return s._init_from_private(key_or_key_literal.priv())

            if keytype==PUBLIC:
                return s._init_from_public(key_or_key_literal.publ())

            return s._init_from_addr(key_or_key_literal.addr)


        # not an object -> initialise from key literal
        if impl.is_privkey(key_or_key_literal):
            return s._init_from_private(key_or_key_literal)

        if impl.is_pubkey(key_or_key_literal):
            return s._init_from_public(key_or_key_literal)

        return s._init_from_addr(key_or_key_literal)

        raise WrongKeyFormat("Key is not in a recognised format {}".format(key_literal))


    ###################################
    ## INIT FROM PRIVATE (private)
    def _init_from_private(s, privkey_literal):
        """
        initialise from private key literal
        """
        s._priv = impl.privkey_decode(privkey_literal)
        s._publ = True
        s._addr = True
        s._type = PRIVATE


    ###################################
    ## INIT FROM PUBLIC (private)
    def _init_from_public(s, pubkey_literal):
        """
        initialise from public key literal
        """
        s._priv = None
        s._publ = impl.pubkey_decode(pubkey_literal)
        s._addr = True
        s._type = PUBLIC

    ###################################
    ## INIT FROM ADDR (private)
    def _init_from_addr(s, addr):
        """
        initialise from address
        """
        s._priv = None
        s._publ = None
        s._addr = addr
        s._type = ADDR


    ###################################
    ## __REPR__
    def __repr__(s):

        # type
        if   s._type == PRIVATE: key = s.privb58
        elif s._type == PUBLIC:  key = s.publhex
        else:                    key = s.addr

        # testnet or main net
        tn = ", testnet=True" if s._network == TESTNET else ""

        return "{}('{}'{})".format(s.__class__.__name__, key, tn)


    ###################################
    ## __STR__
    def __str__(s):
        tn =  " testnet" if s._network == TESTNET else ""
        return "{0}(addr={2}..., {1}{3})".format(
            s.__class__.__name__, s._TYPE_NAMES[s._type], s.addr[:8], tn)

    ###################################
    ## KEYTYPE (property)
    @property
    def keytype(s):
        """
        type of the key (ADDRESS, PUBLIC or PRIVATE)
        """
        return s._type

    ###################################
    ## KEYTYPENM (property)
    @property
    def keytypenm(s):
        """
        human-readable typename of the key
        """
        return s._TYPE_NAMES[s._type]

    ###################################
    ## IS TESTNET (property)
    @property
    def is_testnet(s):
        """
        returns True if Testnet address, False otherwise
        """
        return s._network == TESTNET

    ###################################
    ## NETWORKNM (property)
    @property
    def networknm(s):
        """
        returns human readable name for network (main or testnet)
        """
        return s._NETWORK_NAMES[s._network]


    ###################################
    ## PRIV
    def priv(s, formt=None, raise_if_none=True):
        """
        returns the private key in a given format
        """
        if formt == None: formt = WIFC
        if s._priv is None:
            if raise_if_none:
                raise WrongKeyType ("Key does not have a private key")
            else:
                return None
        magicbyte = 111 if s.is_testnet else 0
        return impl.privkey_encode(s._priv, formt, magicbyte)

    ###################################
    ## PRIVB58 (property)
    @property
    def privb58(s, formt=None):
        """
        returns the private key in b58 compressed format
        """
        return s.priv()

    ###################################
    ## PUBL
    def publ(s, formt=None, raise_if_none=True):
        """
        returns the public key in a given format
        """
        if formt == None: formt = HEX

        # public key None means it is not present and can't be derived
        if s._publ is None:
            if raise_if_none:
                raise WrongKeyType ("Key does not have a public key")
            else: return None

        # public key True means it is not present but can be derived from private key
        if s._publ is True:
            s._publ = impl.pubkey_from_privkey(s._priv)

        return impl.pubkey_encode(s._publ, formt)

    ###################################
    ## PUBLHEX (property)
    @property
    def publhex(s, formt=None):
        """
        returns the public key in hex format
        """
        return s.publ()

    ###################################
    ## ADDR (property)
    @property
    def addr(s):
        """
        returns the address
        """

        # address None means it is not present and can not be derived
        if s._addr is None:
            raise WrongKeyType ("Key does not have an address")

        # address True means it is not present but can be derived from public key
        if s._addr is True:
            magicbyte = 111 if s.is_testnet else 0
            publ = s.publ(BIN)
            s._addr = impl.address_from_pubkey(publ, magicbyte)

        return s._addr

    ###################################
    ## URL BEX (property)
    @property
    def url_bex(s):
        """
        the URL of the wallet on the BlockExplorer site
        """
        tn = "testnet." if s.is_testnet else ""
        url = "https://{}blockexplorer.com/address/{}".format(tn, s.addr)
        return url

    ###################################
    ## URL BCY (property)
    @property
    def url_bcy(s):
        """
        the URL of the wallet on the BlockCypher site
        """
        tn = "-testnet" if s.is_testnet else ""
        url = "https://live.blockcypher.com/btc{}/address/{}/".format(tn, s.addr)
        return url




######################################################################
## CLASS KEY CHAIN
class KeyChain():
    """
    a storage for key object
    """


    ###################################
    ## CONSTRUCTOR
    def __init__(s):
        """
        constructor
        """
        s._keychain = OrderedDict()

    ###################################
    ## MAGIC
    def __len__(s):         return len(s._keychain)
    def __iadd__(s, x):     return s.add(x)

    ###################################
    ## GET
    def get(s, addr, raise_if_not_present=True):
        """
        get the key with address `addr`

        NOTE: raises exception or returns None if not present
        """

        # get a single key
        if isinstance(addr, str):
            try: return s._keychain[addr]
            except KeyError:
                if not raise_if_not_present: return None
                raise KeyNotInChain("Key not present (key={})".format(addr))

        # multiple keys
        elif isinstance(addr, Iterable):
            addrs = (s.get(addri, raise_if_not_present) for addri in addr)
            return list(addrs)

        # key?
        elif isinstance(addr, Key):
            # so that a key can be given as argument
            return s.get(addr.addr, raise_if_not_present)

        # error
        else:
            raise WrongParameters("Not a valid address format (addr={})".format(addr))



    ###################################
    ## ADD
    def add(s, key_or_key_literal):
        """
        add the key / list of keys to the key chain
        """
        if isinstance(key_or_key_literal, str):
            key = Key(key_or_key_literal)

        elif isinstance(key_or_key_literal, Iterable):
            for k in key_or_key_literal: s.add(k)

        # key?
        elif isinstance(key_or_key_literal, Key):
            key = key_or_key_literal

        # none of the above
        else:
            raise WrongParameters("Not a recognised key format (key={})".format(key_or_key_literal))

        # add the key
        s._keychain[key.addr] = key

        # for chaining, and so that += works
        return s


    ###################################
    ## REMOVE
    def remove(s, key_or_key_literal):
        """
        remove the key / list of keys from the key chain
        """

        # address on keychain?
        if key_or_key_literal in s._keychain:
            del s._keychain[key_or_key_literal]
            return

        # key literal?
        elif isinstance(key_or_key_literal, str):
            key = Key(key_or_key_literal)

        # iterable?
        elif isinstance(key_or_key_literal, Iterable):
            for k in key_or_key_literal: s.remove(k)

        # key?
        elif isinstance(key_or_key_literal, Key):
            key = key_or_key_literal

        # none of the above
        else:
            raise WrongParameters("Not a recognised key format (key={})".format(key_or_key_literal))

        # delete the key
        del s._keychain[key.addr]


    ###################################
    ## ADDRS (property)
    @property
    def addrs(s):
        """
        returns iterable of addresses of keys in chain
        """
        addresses = (k for k in s._keychain.keys())
        addresses = list(addresses)
        return addresses
