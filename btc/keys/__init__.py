"""
Bitcoin key management


Defines an object representing various types of bitcoin keys. Implementation
code based on Vitalik Buterin's `pybitcointools` library.

The main submodule is `keys` from which all public symbols are imported.

USAGE

    import keys
    k = keys.Key.from_passphrase("my brainwallet passphrase")
    k.priv()
    k.publ()
    k.addr()

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""
__version__="0.1.2"

from .keys import *
