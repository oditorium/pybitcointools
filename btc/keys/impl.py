"""
Implementation functions for bitcoin key management

Usually you would not need to call those functions directly, they
are all wrapped within the `Key` class. They are based on Vitalik
Buterin's `pybitcointools` toolkit.

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""
from ..const import *
from ..technical import *

import logging
logger = logging.getLogger(__name__)

# NOTE: the magicbytes business is a bit confusing; see
# https://en.bitcoin.it/wiki/Base58Check_encoding#Version_bytes
# for some more details

####################################################################
## TYPES
_STRING_TYPES = (str)
_STRING_OR_BYTES_TYPES = (str, bytes)
_INT_TYPES = (int, float)


####################################################################
## PRIVATE KEYS

####################################
## PRIVKEY GET FORMAT
def privkey_get_format(priv):
    """
    determine format of a private key literal
    """
    # called get_privkey_format in Vitalik's code

    if isinstance(priv, _INT_TYPES):        return DEC

    if not isinstance(priv, _STRING_OR_BYTES_TYPES):
        raise WrongPrivateKeyFormat("Wrong key format (not string, byte, or int)")

    if len(priv) == 32:                     return BIN
    if len(priv) == 33:                     return BINC

    if not isinstance(priv, _STRING_TYPES):
        raise WrongPrivateKeyFormat("Unreckognised key format (wrong byte length)")

    if len(priv) == 64:                     return HEX
    if len(priv) == 66:                     return HEXC

    bin_p = b58check_to_bin(priv)
    if len(bin_p) == 32:                    return WIF
    if len(bin_p) == 33:                    return WIFC

    raise WrongPrivateKeyFormat("Unreckognised key format")


####################################
## PRIVKEY DECODE
def privkey_decode(priv, formt=None):
    """
    decodes the private key into decimal format (implies format if not present)
    """
    # called decode_privkey in Vitalik's code

    if formt is None: formt = privkey_get_format(priv)
    if formt == DEC:        return priv
    elif formt == BIN:      return decode(priv, 256)
    elif formt == BINC:     return decode(priv[:32], 256)
    elif formt == HEX:      return decode(priv, 16)
    elif formt == HEXC:     return decode(priv[:64], 16)
    elif formt == WIF:      return decode(b58check_to_bin(priv),256)
    elif formt == WIFC:
        return decode(b58check_to_bin(priv)[:32],256)
    else: raise WrongPrivateKeyFormat("Unknown key format parameter {}".format(formt))


####################################
## PRIVKEY ENCODE
def privkey_encode(priv, formt=HEX, magicbyte=0):
    """
    encodes a private key (can be in arbitrary format, but DEC is best)

    magicbyte is a paramter that is needed for B58 encode only, to
    distinguish between the main net and the test net (magicbyte=111)
    """
    # called encode_privkey in Vitalik's code

    if privkey_get_format(priv) == DEC: priv_dec = priv
    else: priv_dec = privkey_decode(priv)

    if formt == DEC:        return priv_dec
    elif formt == BIN:      return encode(priv_dec, 256, 32)
    elif formt == HEX:      return encode(priv_dec, 16, 64)
    elif formt == HEXC:     return encode(priv_dec, 16, 64)+'01'
    elif formt == WIF:
        return bin_to_b58check(encode(priv_dec, 256, 32), 128+int(magicbyte))
    elif formt == WIFC:
        return bin_to_b58check(encode(priv_dec, 256, 32)+b'\x01', 128+int(magicbyte))
    else: raise WrongPrivateKeyFormat("Unknown key format parameter {}".format(formt))


####################################
## IS PRIVKEY
def is_privkey(key_literal):
    """
    returns True iff the key literal representats a privat key
    """
    # called is_privkey in Vitalik's code

    try:
        privkey_get_format(key_literal)
        return True
    except WrongKeyFormat: return False




####################################################################
## PUBLIC KEYS

####################################
## PUBKEY GET FORMAT
def pubkey_get_format(pub):
    """
    determine format of a public key literal
    """
    # called get_pubkey_format in Vitalik's code

    if isinstance(pub, (tuple, list)):                  return DEC
    if len(pub) == 65 and pub[0] == 4:                  return BIN
    if len(pub) == 130 and pub[0:2] == '04':            return HEX
    if len(pub) == 33 and pub[0] in [2, 3]:             return BINC
    if len(pub) == 66 and pub[0:2] in ['02', '03']:     return HEXC
    if len(pub) == 64:                                  return EBIN
    if len(pub) == 128:                                 return EHEX

    raise WrongPublicKeyFormat("Unreckognised key format")


####################################
## PUBKEY ENCODE
def pubkey_encode(pub, formt):
    """
    encodes a public key (can be in arbitrary format, but DEC is best)
    """
    # called encode_pubkey in Vitalik's code

    if pubkey_get_format(pub) == DEC: pub_dec = pub
    else: pub_dec = pubkey_decode(pub)

    if formt == DEC: return pub_dec
    elif formt == BIN:
        return b'\x04' + encode(pub_dec[0], 256, 32) + encode(pub_dec[1], 256, 32)
    elif formt == BINC:
        return from_int_to_byte(2+(pub_dec[1] % 2)) + encode(pub_dec[0], 256, 32)
    elif formt == HEX:
        return '04' + encode(pub_dec[0], 16, 64) + encode(pub_dec[1], 16, 64)
    elif formt == HEXC:
        return '0'+str(2+(pub_dec[1] % 2)) + encode(pub_dec[0], 16, 64)
    elif formt == EBIN:
        return encode(pub_dec[0], 256, 32) + encode(pub_dec[1], 256, 32)
    elif formt == EHEX:
        return encode(pub_dec[0], 16, 64) + encode(pub_dec[1], 16, 64)
    else:
        raise WrongPublicKeyFormat("Unknown key format parameter {}".format(formt))


####################################
## PUBKEY DECODE
def pubkey_decode(pub, formt=None):
    """
    decodes a public key into DEC format (determines format if None)
    """
    # called decode_pubkey in Vitalik's code

    if formt is None: formt = pubkey_get_format(pub)
    if formt == DEC:        return pub
    elif formt == BIN:      return (decode(pub[1:33], 256), decode(pub[33:65], 256))
    elif formt == BINC:
                            x = decode(pub[1:33], 256)
                            beta = pow(int(x*x*x+A*x+B), int((P+1)//4), int(P))
                            y = (P-beta) if ((beta + from_byte_to_int(pub[0])) % 2) else beta
                            return (x, y)
    elif formt == HEX:      return (decode(pub[2:66], 16), decode(pub[66:130], 16))
    elif formt == HEXC:     return decode_pubkey(safe_from_hex(pub), 'bin_compressed')
    elif formt == EBIN:     return (decode(pub[:32], 256), decode(pub[32:64], 256))
    elif formt == EHEX:     return (decode(pub[:64], 16), decode(pub[64:128], 16))
    else:
        raise WrongPublicKeyFormat("Unknown key format parameter {}".format(formt))


####################################
## PUBKEY FROM PRIVKEY
def pubkey_from_privkey(privkey_dec):
    """
    creates a binary public key from a decimal private key
    """
    # called privkey_to_pubkey or privtopub in Vitalik's code

    if privkey_dec >= kN:
        raise WrongPrivateKeyFormat("Invalid private key {}".format(privkey_dec))
    return fast_multiply(kG, privkey_dec)


####################################
## IS PUBKEY
def is_pubkey(key_literal):
    """
    returns True iff the key literal is a public key
    """
    # called is_pubkey in Vitalik's code

    try:
        pubkey_get_format(key_literal)
        return True
    except WrongKeyFormat: return False




####################################################################
## ADDRESSES

####################################
## ADDRESS FROM PUBKEY
def address_from_pubkey(pubkey_bin, magicbyte=0):
    """
    creates B58c address from BIN public key

    magicbyte = 111 for test net
    """
    # called privkey_to_address or privtoaddr in Vitalik's code

    #if isinstance(pubkey, (list, tuple)): pubkey = encode_pubkey(pubkey, 'bin')
    if len(pubkey_bin) in [66, 130]:
        return bin_to_b58check(
            hash160_bin(binascii.unhexlify(pubkey_bin)), magicbyte)
    return bin_to_b58check(hash160_bin(pubkey_bin), magicbyte)


####################################
## VERSION BYTE FROM ADDRESS
def version_byte_from_addr(addr):
    """
    determines version by from address (raises on invalid checksum)
    """
    # called get_version_byte in Vitalik's code

    leadingzbytes = len(re.match('^1*', addr).group(0))
    data = b'\x00' * leadingzbytes + change_base(addr, 58, 256)
    #assert bin_dbl_sha256(data[:-4])[:4] == data[-4:]
    if not bin_dbl_sha256_hex(data[:-4])[:4] == data[-4:]:
        raise WrongAddressFormat("Checksum error in address ({})".format(addr))
    #return ord(data[0])
    return data[0]


####################################
## IS ADDR
def is_addr(addr):
    """
    returns True iff addr is an address
    """
    # called is_address in Vitalik's code

    ADDR_RE = re.compile("^[123mn][a-km-zA-HJ-NP-Z0-9]{26,33}$")
    return bool(ADDR_RE.match(addr))
