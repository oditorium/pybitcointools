"""
Interface to various bitcoin nodes, including web based services


Implements objects that allow interfacing with various Bitcoin nodes,
including web-based services that expose an API such as blockcain.info

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""
__version__="0.1.1"

from .nodes import *
