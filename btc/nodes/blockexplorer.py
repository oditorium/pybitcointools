"""
Interface to the Blockexplorer API <https://blockexplorer.com/api-ref>

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""
import requests

from .nodes import *

import datetime

import logging
logger = logging.getLogger(__name__)



######################################################################
## MODULE LEVEL FUNCTIONS

##########################################################
## CONVERT TX
def convert_tx(tx_bex):
    """
    converts a BlockExplorer transaction info obtained from api_tx() into standard format

    SAMPLE OUTPUT

        {
            'time': '2017-08-02T19:06:06.577Z',
            'txid': '4715fe6c22ad7d8d99c73f2b99f2a596a26192c4039239791cdfe6dfd253b5ef',
            'blkid': '00000000000003da5a2a619f6f9106804ddb4f505da0a6189a11a1d579fa0d4d',
            'blkix': 1156027,
            'inputs': [
                {'addr': 'mrfsVy9pzd2y8xY3Yh6oDbw9cAMgS8Zobx', 'value': 16399994},
                {'addr': 'mrfsVy9pzd2y8xY3Yh6oDbw9cAMgS8Zobx', 'value': 10000000}
            ],
            'outputs': [
                {'addr': 'mirGyq68HiGr5qNneUhv9U6qmaoGnJSRwU', 'value': 10000000},
                {'addr': 'mrfsVy9pzd2y8xY3Yh6oDbw9cAMgS8Zobx', 'value': 16299993}
            ],
        }

    """

    def coutp(outp):
        # convert outputs (returns None if to be omitted)
        try:
            if not outp['scriptPubKey']['type'] == 'pubkeyhash': return None
        except KeyError: return None
        try:
            if len(outp['scriptPubKey']['addresses']) != 1: return None
        except: return None
        return {
            'addr': outp['scriptPubKey']['addresses'][0],
            'value': int(float(outp['value'])*SATOSHIS_PER_BITCOIN)
        }

    def cinp(inp):
        # convert inputs (returns None if to be omitted)
        return {'addr': inp['addr'], 'value': inp['valueSat']}

    tx = {}
    tx['txid']    = tx_bex['txid']
    tx['blkix']   = tx_bex.get('blockheight',"")
    tx['blkid']   = tx_bex.get('blockhash',"")
    tx['time']    = tx_bex.get('time',"")
    tx['outputs'] = [coutp(o) for o in tx_bex['vout']]
    tx['outputs'] = [o for o in tx['outputs'] if not o is None]
    tx['inputs']  = [cinp(i) for i in tx_bex['vin']]
    tx['inputs']  = [i for i in tx['inputs'] if not i is None]

    if not tx['time'] == "":
        tx['time'] = datetime.datetime.utcfromtimestamp(tx['time']).isoformat()+"Z"

    return tx


######################################################################
## BLOCKEXPLORER (class)
class BlockExplorer(BlockchainAPINode):
    """
    accessing the Blockexplorer API  <https://blockexplorer.com/api-ref>
    """

    _URL_MAINNET = "https://blockexplorer.com/api"
    _URL_TESTNET = "https://testnet.blockexplorer.com/api"

    ###################################
    ## CONSTRUCTOR
    def __init__(s, testnet=None):
        """
        constructor
        """
        super().__init__(testnet)



    ######################################################
    ## IMPLEMENTATION OF ABSTRACT BASE CLASS METHODS

    ###################################
    ## SEND TX
    def _send_tx(s, transaction):
        """
        send the transaction to the network (via BlockExplorer API)
        """
        return s.api_tx_send(transaction)

    ###################################
    ## UTXO
    def _utxo(s, addr):
        """
        utxo for a single address (via BlockExplorer API)
        """
        return s.extract(
            s.api_utxo(addr),
            {'satoshis': ['value', int], 'txid:vout': 'output'}
        )


    ###################################
    ## UTXOS
    def _utxos(s, addrs, sorted=True):
        """
        utxo for a multiple addresses (via BlockExplorer API)

        NOTE
        - `addrs` must be an iterable; single addresses are not accepted

        - if `sorted` is falseish, a list of dicts is returned where every
          row corresponds to on utxo for one of the addresses, in no
          particular order

        - if `sorted` is trueish, a dict of list of dicts is returned, where
          the outer dict key is the address, and the lists only contain
          transaction of the respective address
        """
        unspent0 = s.extract(
            s.api_utxos(addrs),
            {'address': None, 'satoshis': ['value', int], 'txid:vout': 'output'}
        )
        if not sorted: return unspent0

        # sort -> put them into unspent[addr] = [tx1, tx2, ...]
        unspent = {}
        for u in unspent0:
            addr = u['address']
            try: unspent[addr] += [u]
            except KeyError: unspent[addr] = [u]

        return unspent

    ###################################
    ## TXS
    def _txs(s, txid):
        """
        basic transaction information on one or multiple transactions
        """
        if not isinstance(txid, str):
            raise NoIterables("multiple txids can not be effciently implemented in BlockCypher")
        txo = s.api_tx(txid)
        #return txo
        return convert_tx(txo)





    ######################################################
    ## API PROPER

    # API info
    def api_info(s):
        """
        Blockexplorer API: general network info (returns dict)
        """
        endpoint = ["status"]
        resp = s._api_get(endpoint=endpoint, params={'q': 'getInfo'})
        return resp.json()['info']

    # API block-index
    def api_block_index(s, height):
        """
        Blockexplorer API: block hash by height (returns hash)
        """
        endpoint = ["block-index", int(height)]
        resp = s._api_get(endpoint=endpoint)
        return resp.json()['blockHash']

    # API block
    def api_block(s, hsh):
        """
        Blockexplorer API: block by hash (returns dict)
        """
        endpoint = ["block", hsh]
        resp = s._api_get(endpoint=endpoint)
        return resp.json()

    # API rawblock
    def api_rawblock(s, hsh):
        """
        Blockexplorer API: raw block by hash (returns raw block)
        """
        endpoint = ["rawblock", hsh]
        resp = s._api_get(endpoint=endpoint)
        return resp.json()['rawblock']

    # API tx (transaction)
    def api_tx(s, txid):
        """
        Blockexplorer API: transaction by hash (returns dict)
        """
        endpoint = ["tx", txid]
        resp = s._api_get(endpoint=endpoint)
        return resp.json()

    # API rawtx (raw transaction)
    def api_rawtx(s, txid):
        """
        Blockexplorer API: transaction by hash (returns raw transaction)
        """
        endpoint = ["rawtx", txid]
        resp = s._api_get(endpoint=endpoint)
        return resp.json()['rawtx']

    # API addr-validate
    def api_addr_validate(s, addr):
        """
        Blockexplorer API: validate address (bool True or False)
        """
        endpoint = ["addr-validate", addr]
        resp = s._api_get(endpoint=endpoint)
        return resp.json()

    # API addr
    def api_addr(s, addr, tx_list=True, cache=True):
        """
        Blockexplorer API: address information (returns dict)
        """
        endpoint = ["addr", addr]
        resp = s._api_get(endpoint=endpoint)
        return resp.json()


    # API tx-addrs (transactions for multiple addresses)
    def api_tx_addrs(s, addrs, frm=None, to=None):
        """
        Blockexplorer API: transaction info for multiple addresses (returns list)
        """
        endpoint = ["addrs", ",".join(addrs), "txs"]
        params={}
        if not frm is None: params['from']= frm
        if not to  is None: params['to']=   to
        resp = s._api_get(endpoint=endpoint, params=params)
        return resp.json()

    # API utxo
    def api_utxo(s, addr, cache=True):
        """
        Blockexplorer API: address utxo information (returns list of dicts)
        """
        endpoint = ["addr", addr, "utxo"]
        resp = s._api_get(endpoint=endpoint)
        return resp.json()

    # API utxos
    def api_utxos(s, addrs):
        """
        Blockexplorer API: utxo information for multiple addresses (returns list of dicts)
        """
        endpoint = ["addrs", ",".join(addrs), "utxo"]
        resp = s._api_get(endpoint=endpoint)
        return resp.json()

    # API txs-block (transaction by block)
    def api_txs_block(s, hsh):
        """
        Blockexplorer API: transactions by block (returns dict)
        """
        endpoint = ["txs"]
        resp = s._api_get(endpoint=endpoint, params={'block': hsh})
        return resp.json()

    # API txs-addr (transaction by addres)
    def api_txs_addr(s, addr):
        """
        Blockexplorer API: transactions by address (returns dict)
        """
        endpoint = ["txs"]
        resp = s._api_get(endpoint=endpoint, params={'address': addr})
        return resp.json()

    # API tx-send (send a transaction)
    def api_tx_send(s, transaction):
        """
        Blockexplorer API: send a transaction (returns txid)
        """
        endpoint = ["tx", "send"]
        data = "rawtx: {}".format(transaction)
        #params={'rawtx': transaction}
        resp = s._api_post(endpoint=endpoint, data=data, params=None)
        #return resp
        return resp.json()
