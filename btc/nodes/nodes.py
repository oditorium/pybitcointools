"""
Interface to various bitcoin nodes, including web based services; this
file contains a number of base classes
(central file; do not import directly)


Implements objects that allow interfacing with various Bitcoin nodes,
including web-based services that expose an API such as blockcain.info

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""
#import json
import requests
import urllib.parse as up

from ..const import *
from ..keys import addr
from ..txns import transaction

import logging
logger = logging.getLogger(__name__)


######################################################################
## BLOCKCHAIN NODE (abstract base class)
class BlockchainNode():
    """
    abstract base class for any class connecting to a blockchain node
    """

    ###################################
    ## CONSTRUCTOR
    def __init__(s, testnet=None):
        if testnet is None: testnet = False
        s._testnet = testnet


    ###################################
    ## __REPR__
    def __repr__(s):
        return "{}(testnet={})".format(s.__class__.__name__, s.is_testnet)




    ###################################
    ## IS TESTNET (property)
    @property
    def is_testnet(s):
        """
        True if testnet, False otherwise
        """
        return s._testnet


    ###################################
    ## ASSERT TESTNET
    def assert_testnet(s):
        """
        raises NotOnTestnet exception if not on testnet
        """
        if not s.is_testnet: raise NotOnTestnet("this is not a testnet API")


    ###################################
    ## ASSERT MAINNET
    def assert_mainnet(s):
        """
        raises NotOnMainnet exception if not on main network
        """
        if s.is_testnet: raise NotOnMainnet("this is not a main net API")


    ###################################
    ## EXTRACT (static)
    @staticmethod
    def extract(data, extract):
        """
        extract certain data from `data`, which is a list of dicts

        NOTES
        - `data` is a list of dicts all having the same structure (at least
          what the fields to be extracted are concerned)

        - `extract` is a dict where the key is the field to be extracted,
          and the value its new name (or None, in which case the name is
          not changed)

        - alternatively `extract` can be a list if the names are not to
          be changed

        - if the key is of format 'fn1:fn2', then this corresponds to twp
          separeted fields fn1 and fn2 whose data is concatenated with a
          colon into the output field

        - if the output field is a list, then the first is a field name,
          and the second is a function to be applied, eg ['val', int]
        """

        # list -> make it into a dict with no tag changes
        if isinstance (extract, (list, tuple)):
            extract = {x:x for x in extract}
        # otherwise -> replace None with actual values
        else:
            extract = {x : (v if not v is None else x) for x,v in extract.items()}

        # loop over all the data items (...we could make it a comprehension...)
        result = []
        for d in data:
            # generate the dict and remember
            newd = {}
            for fieldnm, newfield in extract.items():

                # split newfield into field name and function
                # (lambda x: x is the identity function)
                if isinstance(newfield, (list, tuple)):
                    newfn, func = newfield[0], newfield[1]
                else:
                    newfn, func = newfield, lambda x: x

                # if field is of form "aa:bb:cc" combine them with ":"
                fns = fieldnm.split(":")
                newd[newfn] = func(":".join(str(d[fn]) for fn in fns))

            result.append(newd)

        return result

    ######################################################
    ## STATIC METHOD

    ###################################
    ## BALANCE FROM UTXO
    @staticmethod
    def balance_from_utxo(utxo):
        """
        takes utxos (as provided by this class) and calculates total balance
        """
        return sum(o['value'] for o in utxo)



    ######################################################
    ## ABSTRACT PROPERTIES AND METHODS

    ###################################
    ## SEND TX
    def send_tx(s, tx):
        """
        send the transaction to the network

        NOTES
        - transaction can be a transaction string or a MakeTransaction
          object

        - derived classes should overwrite the associated private
          method, not this one
        """
        return s._send_tx(transaction(tx))

    def _send_tx(s, transaction):
        """
        send the transaction to the network (abstract)
        """
        raise NotImplementedError("must be implemented in derived class")


    ###################################
    ## BALANCE
    def balance(s, addr_or_key, as_bitcoin=False):
        """
        unspent balance for single address (usually Satoshis)
        """
        balance = s.balance_from_utxo(s.utxo(addr_or_key))
        if as_bitcoin: return float(balance) / SATOSHIS_PER_BITCOIN
        return balance

    ###################################
    ## UTXO
    def utxo(s, addr_or_key):
        """
        utxo for a single address

        NOTES
        - addr can be an address string or a Key object

        - derived classes should overwrite the associated private
          method, not this one
        """
        return s._utxo(addr(addr_or_key))

    def _utxo(s, addr):
        """
        utxo for a single address (abstract)
        """
        raise NotImplementedError("must be implemented in derived class")

    ###################################
    ## UTXOS
    def utxos(s, addrs, sorted=True):
        """
        utxo for a multiple addresses

        NOTES
        - `addrs` must be an iterable; single addresses are not accepted;
          the constituents can either be address strings or Key objects

        - if `sorted` is falseish, a list of dicts is returned where every
          row corresponds to on utxo for one of the addresses, in no
          particular order

        - if `sorted` is trueish, a dict of list of dicts is returned, where
          the outer dict key is the address, and the lists only contain
          transaction of the respective address

        - derived classes should overwrite the associated private
          method, not this one
        """
        return s._utxos([addr(a) for a in addrs], sorted)

    def _utxos(s, addrs, sorted=True):
        """
        utxo for a multiple addresses (abstract)
        """
        raise NotImplementedError("must be implemented in derived class")


    ###################################
    ## TXS
    def txs(s, txids):
        """
        basic transaction information on one or multiple transactions

        SAMPLE OUTPUT

            # https://testnet.blockexplorer.com/api/tx/4715fe6c22ad7d8d99c73f2b99f2a596a26192c4039239791cdfe6dfd253b5ef

            {
                'time': '2017-08-02T19:06:06.577Z',
                'txid': '4715fe6c22ad7d8d99c73f2b99f2a596a26192c4039239791cdfe6dfd253b5ef',
                'blkid': '00000000000003da5a2a619f6f9106804ddb4f505da0a6189a11a1d579fa0d4d',
                'blkix': 1156027,
                'inputs': [
                    {'addr': 'mrfsVy9pzd2y8xY3Yh6oDbw9cAMgS8Zobx', 'value': 16399994},
                    {'addr': 'mrfsVy9pzd2y8xY3Yh6oDbw9cAMgS8Zobx', 'value': 10000000}
                ],
                'outputs': [
                    {'addr': 'mirGyq68HiGr5qNneUhv9U6qmaoGnJSRwU', 'value': 10000000},
                    {'addr': 'mrfsVy9pzd2y8xY3Yh6oDbw9cAMgS8Zobx', 'value': 16299993}
                ],
                'aggr': {
                    'mirGyq68HiGr5qNneUhv9U6qmaoGnJSRwU':  10000000,
                    'mrfsVy9pzd2y8xY3Yh6oDbw9cAMgS8Zobx': -10100001,
                },
            }

        """
        txo = s._txs(txids)

        aggr = {}

        # inputs are counted negative
        for r in txo['inputs']:
            try: aggr[r['addr']] -= r['value']
            except KeyError: aggr[r['addr']] = -r['value']

        # outputs are counted positive
        for r in txo['outputs']:
            try: aggr[r['addr']] += r['value']
            except KeyError: aggr[r['addr']] = r['value']

        # store and return
        txo['aggr'] = aggr
        return txo

    def _txs(txids):
        """
        basic transaction information on one or multiple transactions (abstract)
        """
        raise NotImplementedError("must be implemented in derived class")



######################################################################
## BLOCKCHAIN LOCAL NODE (abstract base class)
class BlockchainLocalNode(BlockchainNode):
    """
    abstract base class for any class connecting to a local blockchain node
    """

    ###################################
    ## CONSTRUCTOR
    def __init__(s, testnet=None):
        super().__init__(testnet)




######################################################################
## BLOCKCHAIN API NODE (abstract base class)
class BlockchainAPINode(BlockchainNode):
    """
    abstract base class for any class connecting to a web API blockchain node
    """

    # must be set by derived classes
    _URL_MAINNET = None
    _URL_TESTNET = None


    ###################################
    ## CONSTRUCTOR
    def __init__(s, testnet=None):
        super().__init__(testnet)
        s._url = s._URL_TESTNET if testnet else s._URL_MAINNET
        if s._url is None:
            raise WrongParameters("URL is None (testnet={})".format(testnet))




    ###################################
    ## URL (property)
    @property
    def url(s):
        """
        the API URL
        """
        return s._url




    ######################################################
    ## API CALLS

    _HTTP_RESPONSES_NOERR = [200]       # http responses indicating success

    ###################################
    ## MAKE ENDPOINT (static, private)
    @staticmethod
    def _make_endpoint(endpoint):
        """
        create an endpoint relative URL
        """
        if endpoint is None: return ""
        if isinstance(endpoint, str): return endpoint
        return "/".join(str(i) for i in endpoint)


    ###################################
    ## MAKE URL (static, private)
    @staticmethod
    def _make_url(base, endpoint):
        """
        create a URL from a base and an endpoint
        """
        return base + "/" + endpoint


    ###################################
    ## ASSERT RESPONSE OK (classmethod, protected)
    @classmethod
    def _assert_response_ok(cls, response, endpoint_list, endpoint):
        """
        raises an error unless response is OK

        NOTES
        - can be overwritten by derived classes if required

        - `endpoint_list` is the list provided to the function, `endpoint`
          is the actual endpoint; the url is part of `response`
        """
        print(response.url)
        if not response.status_code in cls._HTTP_RESPONSES_NOERR:
            raise ErrorResponse("Request failed (status={}, text='{}')".format(
                                                response.status_code, response.text))


    ###################################
    ## API GET (protected)
    def _api_get(s, endpoint=None, params=None, headers=None):
        """
        call the API using GET (can be used by derived classes)

        NOTES
        - endpoint can either be a string, or a list of strings, in
          which case those are joint for the endpoint URL

        - params are given in a dict, and are converted into GET
          parameters in the usual way

        - headers are given as a dict
        """
        ep = s._make_endpoint(endpoint)
        url = s._make_url(s._url, ep)
        resp = requests.get(url, params=params)
        s._assert_response_ok(resp, endpoint, ep)
        return resp


    ###################################
    ## API POST (protected)
    def _api_post(s, endpoint=None, params=None, data=None, json=None, headers=None):
        """
        call the API using POST (can be used by derived classes)

        NOTES
        - see `_api_get` for endpoint, params, and headers

        - the POST body can be specified either with the `data` or the
          `json` arguments (an exception is raised when both are
          present)

        - `data` can be a dict, a list of tuples (both of which
          are url encode), or a string (which is sent as is);
          `json` is any object and is being sent json encoded
        """
        ep = s._make_endpoint(endpoint)
        url = s._make_url(s._url, ep)
        if all([not data is None, not json is None]):
            raise WrongParameters("cant provide both `data` and `json` arguments")
        #print(ep)
        #print(data)
        #print(params)
        resp = requests.post(url, params=params, data=data, json=json)
        #return resp
        s._assert_response_ok(resp, endpoint, ep)
        return resp



######################################################################
## UNIFIED API
######################################################################
from . import blockexplorer as bx
from . import blockcypher   as by

_API_NODES = {
    BLOCKEXPLORER: [bx.BlockExplorer(), bx.BlockExplorer(testnet=True)],
    BLOCKCYPHER:   [by.BlockCypher(),   by.BlockCypher(testnet=True)  ],
}


###################################
## API GET
def api_get(testnet=None, service=None):
    """
    selected and get an API object

    - if `service` is None, then a service is chosen; the choice of this
      service may change over time, so it is not recommended to rely on
      properties and methods not in the BlockchainAPINode base class

    - if 'testnet' is None, the main net is used
    """
    if testnet is None: testnet = False
    if service is None: service = BLOCKCYPHER

    ix = 1 if testnet else 0
    try:               theapi = _API_NODES[service][ix]
    except KeyError:   theapi  = None

    if theapi is None:
        raise WrongParameters("this API does not exist (service={}, testnet={})".format(service, testnet))

    return theapi


###################################
## API
def api(service=None):
    """
    like api_get, but main net only
    """
    return api_get(testnet=False, service=service)


###################################
## APIT
def apit(service=None):
    """
    like api_get, but testnet only
    """
    return api_get(testnet=True, service=service)
