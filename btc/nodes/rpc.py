"""
simple RPC client tools

NOTES
- for JSON RPC specs see <http://www.jsonrpc.org/specification>
"""
__version__ = "0.1.1"
__version_dt__ = "August 2017"

import json
import requests
from requests.auth import HTTPBasicAuth

import logging
logger = logging.getLogger(__name__)


######################################################################
## JSON RPC REQUEST OBJ (class)
class JsonRPCRequestObj():
    """
    represents a JSON RPC v2.0 Request Object
    """
    def __init__(s):
        pass

    CLIENT_ID = 'pyjrpc'
    VERSION = '2.0'

    def request_object(s, method, params=None):
        """
        creates an RPC request object
        """
        req = {
            'version': s.VERSION,
            'id': s.CLIENT_ID,
            'method': method,
        }
        if not params is None:
            req['params'] = params
        return req

    def __getattr__(s, method):
        """
        implements generic RPC calls
        """
        def func (*args, **kwargs):
            #print (args)
            #print (kwargs)
            if len(args) == 0:
                if len(kwargs) > 0: params = kwargs
                else: params = []
            elif len(kwargs) == 0: params = list(args)
            else:
                params = list(args) + [kwargs]
            return s.request_object(method, params)
        return func


######################################################################
## JSON RPC RESPONSE OBJ (class)

class JsonRPCError(RuntimeError): pass
class JsonRPCRemoteError(JsonRPCError): pass

class JsonRPCResponseObj():
    """
    represents a JSON RPC v2.0 Response Object
    """
    def __init__(s, response):

        if isinstance(response, requests.models.Response):
            response = response.text

        if isinstance(response, dict):
            s._response_obj = response

        elif isinstance(response, str):
            try:
                s._response_obj = json.loads(response)
            except:
                raise JsonRPCError("can't json decode response (response='{}')".format(response))

        else:
            raise JsonRPCError("unknown response (response='{}')".format(response))


        # well-formed JSON RPC object should not have both
        try:
            if s._response_obj['result'] is None: del s._response_obj['result']
        except: pass
        try:
            if s._response_obj['error'] is None: del s._response_obj['error']
        except: pass

        if not 'result' in s._response_obj and not 'error' in s._response_obj:
            s._response_obj['result'] = {}




    @property
    def is_ok(s):
        """
        returns True if the response is OK, False if error
        """
        return 'result' in s._response_obj

    @property
    def is_error(s):
        """
        returns True if the response is error, False if OK
        """
        return 'error' in s._response_obj

    @property
    def response(s):
        """
        returns the response object
        """
        return s._response_obj

    @property
    def result(s):
        """
        returns the result (raises if there was an error response)
        """
        if not s.is_ok:
            raise JsonRPCRemoteError("received RPC error response (error={})".format(s._response_obj['error']))
        return s._response_obj['result']

    @property
    def ri(s):
        """
        returns the result in interactive mode (prints an error response if need be)
        """
        if not s.is_ok:
            print("ERROR: {0[message]}\n(code={0[code]})".format(s._response_obj['error']))
            return None
        return s._response_obj['result']

    @property
    def error(s):
        """
        returns the error (returns None if there was no error)
        """
        if not s.is_error: return None
        return s._response_obj['error']




######################################################################
## JSON RPC PROXY (class)
class JsonRPCProxy():
    """
    a simple proxy to contact JSON RPC v2.0 servers

    USAGE

        proxy = JsonRPCProxy("http://localhost:18332", 'user', 'pw')
        proxy.getinfo()
        proxy.listreceivedbyaddress(1)

    """

    REQUEST_OBJECT_CLS  = JsonRPCRequestObj
    RESPONSE_OBJECT_CLS = JsonRPCResponseObj


    def __init__(s, url, user=None, pw=None):
        s._url = url
        if user is None and pw is None:
            s._auth = None
        else:
            if pw is None: pw = ""
            s._auth = HTTPBasicAuth(user, pw)

    @property
    def url(s):
        return s._url

    def make_request(s, request_object):
        """
        makes a JSON RPC request

        NOTES
        - request_object is a well-formatted Python object, for example generated
          using the JsonRPCRequestObj class
        """
        request_json = json.dumps(request_object)
        response = requests.post(s.url, request_json, auth=s._auth)

        return s.RESPONSE_OBJECT_CLS(response)

    def __getattr__(s, method):
        """
        implements generic RPC calls
        """
        def func (*args, **kwargs):
            #print (args)
            #print (kwargs)
            request_obj = s.REQUEST_OBJECT_CLS().__getattr__(method)(*args, **kwargs)
            return s.make_request(request_obj)

        return func
