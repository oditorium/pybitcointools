"""
Interface to the Blockcypher API
<https://www.blockcypher.com/dev/bitcoin/#restful-resources>

(c) Copyright Stefan LOESCH 2017. All Rights Reserved.
LICENSE: Mozilla Public License, v2.0 <https://mozilla.org/MPL/2.0/>
"""
import requests

from .nodes import *

import logging
logger = logging.getLogger(__name__)


######################################################################
## MODULE LEVEL FUNCTIONS

##########################################################
## CONVERT TX
def convert_tx(tx_bcy):
    """
    converts a BlockCypher transaction info obtained from api_txs() into standard format

    SAMPLE OUTPUT

        {
            'time': '2017-08-02T19:06:06.577Z',
            'txid': '4715fe6c22ad7d8d99c73f2b99f2a596a26192c4039239791cdfe6dfd253b5ef',
            'blkid': '00000000000003da5a2a619f6f9106804ddb4f505da0a6189a11a1d579fa0d4d',
            'blkix': 1156027,
            'inputs': [
                {'addr': 'mrfsVy9pzd2y8xY3Yh6oDbw9cAMgS8Zobx', 'value': 16399994},
                {'addr': 'mrfsVy9pzd2y8xY3Yh6oDbw9cAMgS8Zobx', 'value': 10000000}
            ],
            'outputs': [
                {'addr': 'mirGyq68HiGr5qNneUhv9U6qmaoGnJSRwU', 'value': 10000000},
                {'addr': 'mrfsVy9pzd2y8xY3Yh6oDbw9cAMgS8Zobx', 'value': 16299993}
            ],
        }

    """

    def coutp(outp):
        # convert outputs (returns None if to be omitted)
        if not outp['script_type'] == 'pay-to-pubkey-hash': return None
        try:
            if len(outp['addresses']) != 1: return None
        except: return None
        return {'addr': outp['addresses'][0], 'value': outp['value']}

    def cinp(inp):
        # convert inputs (returns None if to be omitted)
        try:
            if len(inp['addresses']) != 1: return None
        except: return None
        return {'addr': inp['addresses'][0], 'value': inp['output_value']}

    tx = {}
    tx['txid']    = tx_bcy['hash']
    tx['blkix']   = tx_bcy.get('block_height',"")
    tx['blkid']   = tx_bcy.get('block_hash',"")
    tx['time']    = tx_bcy.get('received',"")
    #tx['inputs']  = [{'addr': inp['addresses'][0]} for inp in tx_bcy['inputs'] ]
    tx['outputs'] = [coutp(o) for o in tx_bcy['outputs']]
    tx['outputs'] = [o for o in tx['outputs'] if not o is None]
    tx['inputs']  = [cinp(i) for i in tx_bcy['inputs']]
    tx['inputs']  = [i for i in tx['inputs'] if not i is None]

    return tx






######################################################################
## BLOCKCYPHER (class)
class BlockCypher(BlockchainAPINode):
    """
    accessing the Blockcypher API
    <https://www.blockcypher.com/dev/bitcoin/#restful-resources>
    """

    _URL_MAINNET = "https://api.blockcypher.com/v1/btc/main"
    _URL_TESTNET = "https://api.blockcypher.com/v1/btc/test3"
    _HTTP_RESPONSES_NOERR = [200, 201]


    ###################################
    ## CONSTRUCTOR
    def __init__(s, testnet=None):
        """
        constructor
        """
        super().__init__(testnet)



    ######################################################
    ## IMPLEMENTATION OF ABSTRACT BASE CLASS METHODS

    ###################################
    ## SEND TX
    def _send_tx(s, transaction):
        """
        send the transaction to the network (via BlockCypher API)
        """
        return s.api_txs_push(transaction)

    ###################################
    ## UTXO
    def _utxo(s, addr):
        """
        utxo for a single address (via BlockCypher API)
        """
        raw_data = s.api_addrs(addr, unspentOnly=True)
        if not 'txrefs' in raw_data: return []
        formt = {'value': ['value', int], 'tx_hash:tx_output_n': 'output'}
        return s.extract(raw_data['txrefs'], formt)

    ###################################
    ## UTXOS
    def _utxos(s, addrs, sorted=True):
        """
        NOT IMPLEMENTED -- utxo for a multiple addresses (via BlockCypher API)
        """
        raise NotImplementedError("this method can not be effciently implemented in BlockCypher")

    ###################################
    ## TXS
    def _txs(s, txid):
        """
        basic transaction information on one or multiple transactions
        """
        if not isinstance(txid, str):
            raise NoIterables("multiple txids can not be effciently implemented in BlockCypher")
        txo = s.api_txs(txid)
        return convert_tx(txo)
        return txo



    ######################################################
    ## API PROPER

    # API blocks (block by height)
    def api_blocks(s, height):
        """
        Blockcypher API: block by height (returns dict)
        """
        endpoint = ["blocks", int(height)]
        resp = s._api_get(endpoint=endpoint)
        return resp.json()

    # API balance (basic address information)
    def api_balance(s, addr):
        """
        Blockcypher API: basic address information (returns dict)
        """
        endpoint = ["addrs", addr, "balance"]
        resp = s._api_get(endpoint=endpoint)
        return resp.json()

    # API addrs (address information)
    def api_addrs(s, addr, unspentOnly=None, includeScript=None,
            includeConfidence=None, before=None, after=None,
            limit=None, confirmations=None, confidence=None,
            omitWalletAddresses=None):
        """
        Blockcypher API: address information (returns dict)
        """
        endpoint = ["addrs", addr]
        params = {}
        if not unspentOnly is None: params['unspentOnly'] = int(unspentOnly)
        if not includeScript is None: params['includeScript'] = int(includeScript)
        if not includeConfidence is None: params['includeConfidence'] = int(includeConfidence)
        if not before is None: params['before'] = int(before)
        if not after is None: params['after'] = int(after)
        if not limit is None: params['limit'] = int(limit)
        if not confirmations is None: params['confirmations'] = int(confirmations)
        if not confidence is None: params['confidence'] = int(confidence)
        if not omitWalletAddresses is None: params['omitWalletAddresses'] = int(omitWalletAddresses)
        resp = s._api_get(endpoint=endpoint, params=params)
        return resp.json()

    # API addrs-full (full address information)
    def api_addrs_full(s, addr, includeScript=None, includeConfidence=None,
            includeHex=None, before=None, after=None, limit=None,
            txlimit=None, confirmations=None, confidence=None,
            omitWalletAddresses=None):
        """
        Blockcypher API: full address information (returns dict)
        """
        endpoint = ["addrs", addr, "full"]
        params = {}
        if not includeScript is None: params['includeScript'] = int(includeScript)
        if not includeConfidence is None: params['includeConfidence'] = int(includeConfidence)
        if not includeHex is None: params['includeHex'] = int(includeHex)
        if not before is None: params['before'] = int(before)
        if not after is None: params['after'] = int(after)
        if not limit is None: params['limit'] = int(limit)
        if not txlimit is None: params['txlimit'] = int(txlimit)
        if not confirmations is None: params['confirmations'] = int(confirmations)
        if not confidence is None: params['confidence'] = int(confidence)
        if not omitWalletAddresses is None: params['omitWalletAddresses'] = int(omitWalletAddresses)
        resp = s._api_get(endpoint=endpoint, params=params)
        return resp.json()

    # API txs (transaction information)
    def api_txs(s, txhash, limit=None, instart=None,
                            outstart=None, includeHex=None):
        """
        Blockcypher API: transaction information (returns dict)
        """
        endpoint = ["txs", txhash]
        params = {}
        if not limit is None: params['limit'] = int(limit)
        if not instart is None: params['instart'] = int(instart)
        if not outstart is None: params['outstart'] = int(outstart)
        if not includeHex is None: params['includeHex'] = int(includeHex)
        resp = s._api_get(endpoint=endpoint, params=params)
        return resp.json()

    # API txs-push (push transaction)
    def api_txs_push(s, transaction):
        """
        Blockcypher API: push transaction (returns dict)
        """
        endpoint = ["txs", "push"]
        json={'tx': transaction}
        resp = s._api_post(endpoint=endpoint, json=json)
        return resp.json()
